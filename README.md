A Messenger Application that was made using Java and the framework JavaFX
It uses a PostgreSQL database for storing everything
It features a Login/Signup system , with hashed passwords using SHA-256 and SALT
It lets people login from the same computer and communicate between them in real-time
