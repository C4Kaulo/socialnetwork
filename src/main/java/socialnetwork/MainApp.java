package socialnetwork;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.loginScreenController;
import socialnetwork.domain.Password;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.domain.messages.Message;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.FriendshipValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.UtilizatorService;

import java.io.File;
import java.io.IOException;

public class MainApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

        Repository<Long, Utilizator> userDbRepository =
                new DbUserRepository(url,username, pasword,  new UtilizatorValidator());

        Repository<Long , Prietenie> friendshipRepository =
                new DbFrienshipRepository(url , username , pasword , new FriendshipValidator());

        DbUserMessagerRepository recipientRepository =
                new DbUserMessagerRepository(url , username , pasword , new FriendshipValidator());

        Repository<Long , Message> messageRepository =
                new DbMessageRepository(url , username , pasword , new FriendshipValidator());

        Repository<Long , FriendRequest> friendRequestRepository =
                new DbFriendRequestRepository(url , username , pasword , new FriendshipValidator());

        Repository<Long , Password> passwordRepository =
                new DbUserPassword(url , username , pasword ,new UtilizatorValidator());

        PagingRepository<Long , LiveEvent> liveEventRepository =
                new DbLiveEventRepository(url , username , pasword );

        Repository<Long , EventNotification> eventNotificationRepository =
                new DbUsersToEventsRepository(url , username , pasword);

        UtilizatorService service = new UtilizatorService(userDbRepository ,
                friendshipRepository ,
                messageRepository,
                recipientRepository ,
                friendRequestRepository ,
                passwordRepository ,
                liveEventRepository,
                eventNotificationRepository);

        FXMLLoader loader = null;
        loader = new FXMLLoader(getClass().getClassLoader().getResource("loginScreen.fxml"));
        loginScreenController controller = new loginScreenController();
        controller.setService(service);
        loader.setController(controller);

        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.setScene(new Scene(loader.load()));

        controller.setPrimaryStage(primaryStage);

        controller.start();
    }
}
