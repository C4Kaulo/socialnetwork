/*package socialnetwork.ui;

import com.sun.tools.jdeprscan.scan.Scan;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.service.UtilizatorService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Ui {
    private UtilizatorService service;

    public Ui(UtilizatorService service)
    {
        this.service = service;
    }

    private void printMenu()
    {
        System.out.println("1.Adauga utilizator\n" +
                "2.Sterge utilizator\n" +
                "3.Adauga prietenie\n" +
                "4.Sterge prietenie\n" +
                "5.Comunitati\n" +
                "6.Comunitatea maximala\n" +
                "7.Toate prieteniile unui utilizator\n" +
                "8.Toate prieteniile unui utilizator intr-o luna\n" +
                "9.Creeaza mesaj\n"+
                "10.Reply mesaj\n" +
                "11.Istoric conversatie\n"+
                "12.Trimite cerere de prietenie\n"+
                "13.Manage cereri de prietenie\n");
    }

    private void solve13()
    {

        Scanner input = new Scanner(System.in);
        Long u1 , u2 , re;
        System.out.print("Da-mi id-ul user-ului : ");
        u1 = input.nextLong();

        service.getAllFriendRequests(u1).forEach(x-> System.out.println(x.getEntity() + ")Friend request de la " +x.getFrom() + " pentru " +x.getTo()+ " cu statusul " +x.getStatus()));

        System.out.print("Da-mi id-ul cererii de prietenie : ");
        u2 = input.nextLong();
        System.out.println("Seteaza status\n"+
                "1)PENDING\n2)APPROVED\n3)REJECTED");
        re = input.nextLong();
    }

    private void solve10()
    {
        Scanner input = new Scanner(System.in);
        Long id , idM , idU ;
        String content;
        System.out.print("Da-mi id-ul : ");
        id = input.nextLong();
        System.out.print("Da-mi id-ul user-ului : ");
        idU = input.nextLong();

        System.out.println("0)La toate");
        service.getAllMessagesForUser(idU).forEach(x-> System.out.println(x.getEntity()+")La ora " + x.getDate() + "trimis de " + x.getFrom() + " cu continutul " + x.getContent()));
        System.out.print("Carui mesaj vrei sa raspunzi : ");
        idM = input.nextLong();

        input.nextLine();
        System.out.println("Ce vrei sa pui in mesaj : ");
        input.nextLine();
        content = input.nextLine();
        if (idM == 0)
        {
            service.replyAllMessage(id,idM , idU , content);
        }
        else
        {
            System.out.print("Carui utilizator ii raspunzi : ");
            Long idUsent = input.nextLong();
            service.replyOneMessage(id , idM , idU ,idUsent,content);
        }
    }

    private void solve11()
    {
        Scanner input = new Scanner(System.in);
        Long u1 , u2;
        System.out.print("Da-mi id-ul primului user : ");
        u1 = input.nextLong();
        System.out.print("Da-mi id-ul al doilea user : ");
        u2 = input.nextLong();

        service.conversation(u1 , u2).forEach(x-> System.out.println("La ora " + x.getDate() + "trimis de " + x.getFrom() + " cu continutul " + x.getContent()));
    }

    private void solve12()
    {
        Scanner input = new Scanner(System.in);

        Long id, u1 , u2;
        System.out.print("Da-mi id-ul cererii pe care o creezi : ");
        id = input.nextLong();
        System.out.print("Da-mi id-ul primului user : ");
        u1 = input.nextLong();
        System.out.print("Da-mi id-ul al doilea user : ");
        u2 = input.nextLong();
        service.sendFriendRequest(u1 , u2);
    }

    private void solve1()
    {
        Long ID;
        Scanner input = new Scanner(System.in);
        String first_name , last_name;
        System.out.print("Da-mi ID-ul : ");
        ID = input.nextLong();
        System.out.println();
        input.nextLine();
        System.out.print("Da-mi numele : ");
        first_name = input.nextLine();
        System.out.print("Da-mi prenumele : ");
        last_name = input.nextLine();
        service.addUtilizator(new Utilizator(ID , first_name , last_name));
    }

    private void solve2()
    {
        Scanner input = new Scanner(System.in);
        Long id_delete;
        System.out.print("Da-mi id-ul user-ului pe care vrei sa il elimini : ");
        id_delete = input.nextLong();
        System.out.println();
        service.removeUtilizator(id_delete);
    }

    private void solve3()
    {
        Scanner input = new Scanner(System.in);
        Long id_user , id_friend;
        System.out.print("Da-mi id-ul user-ului : ");
        id_user = input.nextLong();
        System.out.println();
        System.out.print("Da-mi id-ul prietenului : ");
        id_friend = input.nextLong();
        service.addPrietenUtilizator(id_user , id_friend);
    }

    private void solve4()
    {
        Scanner input = new Scanner(System.in);
        Long id_user , id_friend;
        System.out.print("Da-mi id-ul user-ului : ");
        id_user = input.nextLong();
        System.out.println();
        System.out.print("Da-mi id-ul prietenului : ");
        id_friend = input.nextLong();
        service.removeFriendUtilizator(id_user , id_friend);
    }

    private void solve5()
    {
        System.out.println(service.comunitate());
    }

    private void solve6()
    {
        System.out.println("Comunitatile sau comunitatea cu numar maxim de prietenii este :");
        System.out.println(service.maxComunitate());
    }

    private void printRepo()
    {
        service.getAll().forEach(x-> System.out.println(x));
    }

    private void solve7()
    {
        Scanner input = new Scanner(System.in);
        Long id_user ;
        System.out.print("Da-mi id-ul utilizatorului : ");
        id_user = input.nextLong();
        service.allFriendships(id_user).forEach(x-> System.out.println(x));
    }

    private void solve8()
    {
        Scanner input = new Scanner(System.in);
        Long id_user ;
        int month;
        System.out.print("Da-mi id-ul utilizatorului : ");
        id_user = input.nextLong();
        System.out.print("Da-mi luna : ");
        month = input.nextInt();
        service.allFriendshipsInAMonth(id_user , month).forEach(x-> System.out.println(x));
    }
    private void solve9()
    {
        List<Long> to = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        Long n;
        Long id , idFrom ;
        String content;
        System.out.print("Da-mi id-ul mesajului : ");
        id = input.nextLong();

        System.out.print("Id-ul userului de la care vine mesajul : ");
        idFrom = input.nextLong();

        System.out.print("La cati useri vrei sa trimiti mesajul : ");
        n = input.nextLong();
        for (Long i = 0L; i < n; i++)
        {
            to.add(input.nextLong());
        }

        input.nextLine();
        System.out.println("Ce vrei sa contina mesajul : ");
        content = input.nextLine();

        //service.sendFriendRequest(1L , 1L , 5L);
        //service.getAllMessagesForUser(2L).forEach(x-> System.out.println(x));
        /*to.add(2L);
        to.add(3L);
        service.createMessage(1L , 1L , to , "Boi we are online");
        service.replyAllMessage(2L,1L , 2L , "HEHE BOI");*/
        //service.replyOneMessage(3L , 1L , 3L , 1L , "Message received boss");
        //service.conversation(1L , 3L).forEach(x-> System.out.println("La ora " + x.getDate() + "trimis de " + x.getFrom() + " cu continutul " + x.getContent()));
/*    }

    private void mainMenu()
    {
        System.out.println("Welcome : ");
        while(true)
        {
            printMenu();
            printRepo();
            Scanner input = new Scanner(System.in);
            int choice = input.nextInt();
            try
            {
            switch(choice)
            {
                    case 1 :
                        solve1();
                        break;
                    case 2 :
                        solve2();
                        break;
                    case 3 :
                        solve3();
                        break;
                    case 4 :
                        solve4();
                        break;
                    case 5 :
                        solve5();
                        break;
                    case 6 :
                        solve6();
                        break;
                    case 7 :
                        solve7();
                        break;
                    case 8 :
                        solve8();
                        break;
                    case 9:
                        solve9();
                        break;
                    case 10:
                        solve10();
                        break;
                case 11:
                    solve11();
                    break;
                case 12:
                    solve12();
                    break;
                case 13:
                    solve13();
                    break;
                    default :
                        return;
                }
            }
            catch(ValidationException e)
            {
                System.out.println(e.getMessage());
            }
        }
    }

    public void run()
    {
        mainMenu();
    }
}
*/