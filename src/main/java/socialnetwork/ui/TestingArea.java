package socialnetwork.ui;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.File;

public class TestingArea extends Application {

    public void start(final Stage stage) {
        stage.setTitle("File Chooser Sample");
        final StackPane stac = new StackPane();
        final FileChooser fileChooser = new FileChooser();

        final Button openButton = new Button("Choose Background Image");
        openButton.setOnAction((final ActionEvent e) -> {
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                System.out.println(file.toURI().toString());
                Image image1 = new Image(file.toURI().toString());
                /*
                file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/arrow.jpg
                file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/checkmark.jpg
                file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/rejected.jpg
                 */
                ImageView ip = new ImageView(image1);
                BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
                BackgroundImage backgroundImage = new BackgroundImage(image1, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
                stac.setBackground(new Background(backgroundImage));
            }

        });

        stac.getChildren().add(openButton);
        stage.setScene(new Scene(stac, 500, 500));
        stage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}