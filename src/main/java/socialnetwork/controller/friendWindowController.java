package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.DTOFriendship;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.service.UtilizatorService;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class friendWindowController implements Initializable
{
    DTOUser user;
    DTOUser friend;
    UtilizatorService service;

    @FXML
    Button removeFriendButton;

    @FXML
    Button openChatButton;

    @FXML
    Button groupChatButton;

    @FXML
    Label emailLabel;

    @FXML
    Label usernameLabel;

    public void setUser(DTOUser user) {
        this.user = user;
    }

    public void setFriend(DTOUser friend) {
        this.friend = friend;
    }

    public void setService(UtilizatorService service) {
        this.service = service;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        emailLabel.setText(this.friend.getEmail());
        usernameLabel.setText(this.friend.getFirstName() + " " + this.friend.getLastName());

    }
}
