package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.observer.Observer;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class eventWindowController implements Initializable
{
    DTOUser user;
    UtilizatorService service;
    LiveEvent event;

    ObservableList<DTOUser> observableList = FXCollections.observableArrayList();

    @FXML
    ImageView imageView;

    @FXML
    CheckBox notificationsCheckBox;

    @FXML
    Button subscribeButton;

    @FXML
    TextArea descriptionTextArea;

    @FXML
    Button unsubscribeButton;

    @FXML
    HBox boxButton;

    @FXML
    TextField dateEvent;

    @FXML
    ListView<DTOUser> listViewUsers;

    public void setPrerequisites(UtilizatorService service , DTOUser user , LiveEvent event)
    {
        this.service = service;
        this.user = user;
        this.event = event;
    }

    public void loadUsers()
    {
        event.getSubscribedUsers().forEach(x->observableList.add(x));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        boolean found = false;
        loadUsers();
        for (DTOUser subscribedUser : event.getSubscribedUsers())
        {
            if(subscribedUser.getID().equals(this.user.getID()))
            {
                boxButton.getChildren().add(unsubscribeButton);
                found = true;
            }
        }
        if(!found)
        {
            boxButton.getChildren().add(subscribeButton);
        }
        if(LocalDateTime.now().isAfter(this.event.getDate()))
        {
            subscribeButton.setDisable(true);
        }

        listViewUsers.setOnMouseClicked(x->
        {

        });
        this.descriptionTextArea.setText(event.getDescription());
        this.dateEvent.setText(event.getDate().toString());
        imageView.setImage(new Image(event.getFileLocation()));
        listViewUsers.setItems(observableList);

        this.subscribeButton.setOnMouseClicked(x->
        {
            this.service.subscribeToEvent(user.getID() , event.getId());
            boxButton.getChildren().clear();
            boxButton.getChildren().add(unsubscribeButton);
            observableList.add(user);
        });

        this.unsubscribeButton.setOnMouseClicked(x->
        {
            this.service.unsubscribeFromEvent(user.getID() , event.getId());
            boxButton.getChildren().clear();
            boxButton.getChildren().add(subscribeButton);
            observableList.remove(user);
        });
    }
}
