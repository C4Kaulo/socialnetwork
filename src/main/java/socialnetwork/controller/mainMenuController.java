package socialnetwork.controller;

import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.*;
import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class mainMenuController implements Initializable , Observer<AbstractEvent>
{
    ObservableList<LiveEvent> modelEvent = FXCollections.observableArrayList();
    
    ObservableList<DTOFriendship> modelGrade = FXCollections.observableArrayList();

    ObservableList<DTOFriendRequest> modelRequest=FXCollections.observableArrayList();

    ObservableList<DTOUser> modelUser=FXCollections.observableArrayList();

    ObservableList<DTOMessage> modelMessage = FXCollections.observableArrayList();

    Stage stage;
    DTOUser currentUser;
    UtilizatorService service;

    @FXML
    TabPane tabPane;

    @FXML
    Label labelLastName;

    //-----------------Friend List Tab

    @FXML
    TableView<DTOFriendship> tableFriendList;
    @FXML
    TableColumn<DTOFriendship , Long> tableColumnFLID;
    @FXML
    TableColumn<DTOFriendship , String> tableColumnFLfirstName;
    @FXML
    TableColumn<DTOFriendship , String> tableColumnFLlastName;
    @FXML
    TableColumn<DTOFriendship , LocalDateTime> tableColumnFLdate;


    //-----------------Friend Request Tab

    @FXML
    TableView<DTOFriendRequest> tableFriendRequest;

    @FXML
    TableColumn<DTOFriendRequest, Long> tableColumnFRID;

    @FXML
    TableColumn<DTOFriendRequest , Long> tableColumnFRToID;

    @FXML
    TableColumn<DTOFriendRequest, Long> tableColumnFRFromID;

    @FXML
    TableColumn<DTOFriendRequest, String > tableColumnFRName;

    @FXML
    TableColumn<DTOFriendRequest, Long> tableColumnFRStatus;


    //-----------------Users Tab

    @FXML
    TableView<DTOUser> tableUsers;

    @FXML
    TableColumn<DTOUser, Long> tableColumnUID;

    @FXML
    TableColumn<DTOUser, String> tableColumnUFirstName;

    @FXML
    TableColumn<DTOUser, String> tableColumnULastName;

    @FXML
    Label labelFirstName;

    //-----------------Messages
    @FXML
    TableView<DTOMessage> tableMessages;

    @FXML
    TableColumn<DTOMessage , Long> tableMessagesIDChat;

    @FXML
    TableColumn<DTOMessage , Long> tableMessagesWith;

    @FXML
    TableColumn<DTOMessage , Long> tableMessagesName;


    
    @FXML
    Pagination paginator;

    @FXML
    Button generatePDF;

    @FXML
    Button createEvent;

    //-----------------Events
    @FXML
    TableView<LiveEvent> tableEvents;
    @FXML
    TableColumn<LiveEvent , Long> tableEventsID;
    @FXML
    TableColumn<LiveEvent , String> tableEventsTitle;
    @FXML
    TableColumn<LiveEvent , String> tableEventsDescription;

    public void setCurrentUser(DTOUser currentUser)
    {
        this.currentUser = currentUser;
    }

    public void setService(UtilizatorService service)
    {
        this.service = service;
        this.service.addObserver(this);
    }

    private void loadFriendList()
    {
        modelGrade.setAll(
                StreamSupport.stream(service.getAllFriends(currentUser.getID()).spliterator(), false)
                        .collect(Collectors.toList()));
    }

    private void loadFriendRequest()
    {
        modelRequest.setAll(
                StreamSupport.stream(service.getFriendRequest(currentUser.getID()).spliterator(),false)
                        .collect(Collectors.toList())
        );
    }

    private void loadEvents()
    {
        modelEvent.clear();
        modelEvent.setAll(
                StreamSupport.stream(service.getLiveEvents(paginator.getCurrentPageIndex()).spliterator(),false)
                        .collect(Collectors.toList())
        );
    }

    private void loadUsers()
    {
        modelUser.setAll(
          StreamSupport.stream(service.getUsers(currentUser.getID()).spliterator(),false)
                        .collect(Collectors.toList())
        );
    }

    private void loadMessages()
    {
        modelMessage.setAll(
                StreamSupport.stream(service.getMessages(currentUser.getID()).spliterator(),false)
                        .collect(Collectors.toList())
        );
    }
    public void load()
    {
        loadFriendList();
        loadFriendRequest();
        loadUsers();
        loadMessages();
        loadEvents();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        this.labelFirstName.setText(this.currentUser.getFirstName());
        this.labelLastName.setText(this.currentUser.getLastName());

        this.tableColumnFLdate.setCellValueFactory(new PropertyValueFactory<>("date"));
        this.tableColumnFLfirstName.setCellValueFactory(new PropertyValueFactory<>("first_name"));
        this.tableColumnFLlastName.setCellValueFactory(new PropertyValueFactory<>("last_name"));
        this.tableColumnFLID.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableFriendList.setItems(modelGrade);

        this.tableColumnFRID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        this.tableColumnFRFromID.setCellValueFactory(new PropertyValueFactory<>("From_ID"));
        this.tableColumnFRToID.setCellValueFactory(new PropertyValueFactory<>("To_ID"));
        this.tableColumnFRName.setCellValueFactory(new PropertyValueFactory<>("Nume"));
        this.tableColumnFRStatus.setCellValueFactory(new PropertyValueFactory<>("Status"));
        tableFriendRequest.setItems(modelRequest);

        this.tableColumnUID.setCellValueFactory(new PropertyValueFactory<>("ID"));
        this.tableColumnUFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        this.tableColumnULastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        tableUsers.setItems(modelUser);

        tableMessagesIDChat.setCellValueFactory(new PropertyValueFactory<>("id"));
        tableMessagesWith.setCellValueFactory(new PropertyValueFactory<>("withUser"));
        tableMessagesName.setCellValueFactory(new PropertyValueFactory<>("name"));
        tableMessages.setItems(modelMessage);

        this.tableEventsID.setCellValueFactory(new PropertyValueFactory<>("id"));
        this.tableEventsDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
        this.tableEventsTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
        this.tableEvents.setItems(modelEvent);

        load();

        generatePDF.setOnMouseClicked(x->
        {
            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("generatePDF.fxml"));

            activityPdfGeneratorController controller = new activityPdfGeneratorController();
            controller.setPrerequisites(this.service , this.currentUser);
            loader.setController(controller);
            try
            {
                newStage.setScene(new Scene(loader.load()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            newStage.show();
        });
        this.createEvent.setOnMouseClicked(x->
        {
            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("createEvent.fxml"));

            createEventWindowController controller = new createEventWindowController();
            controller.setPrerequisites(this.service , this.currentUser , newStage);
            loader.setController(controller);
            try
            {
                newStage.setScene(new Scene(loader.load()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            newStage.show();
        });

        this.tableUsers.setOnMouseClicked(x->
        {
            Long friendRequestId = null;
            try
            {
                friendRequestId = this.service.getFriendRequestId(this.currentUser.getID() , tableUsers.getSelectionModel().getSelectedItem().getID());
            }
            catch (ServiceException e)
            {
                friendRequestId = null;
            }

            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("userWindow.fxml"));

            userWindowController controller = new userWindowController();
            controller.setService(this.service);
            controller.setUsers(this.currentUser , tableUsers.getSelectionModel().getSelectedItem() , friendRequestId);
            controller.setParentController(this);
            loader.setController(controller);
            try
            {
                newStage.setScene(new Scene(loader.load()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            newStage.show();
        });

        this.tableEvents.setOnMouseClicked(x->
        {
            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("eventWindow.fxml"));

            eventWindowController controller = new eventWindowController();
            controller.setPrerequisites(this.service , this.currentUser , tableEvents.getSelectionModel().getSelectedItem());
            loader.setController(controller);
            try
            {
                newStage.setScene(new Scene(loader.load()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            newStage.show();
        });

        this.paginator.setOnMouseClicked(x->
        {
            System.out.println("Clicked on the paginator cu pagina : " + paginator.getCurrentPageIndex());
            loadEvents();
        });

        this.tableFriendList.setOnMouseClicked(x->
        {
            Long friendRequestId = null;
            try
            {
                friendRequestId = this.service.getFriendRequestId(this.currentUser.getID() , tableFriendList.getSelectionModel().getSelectedItem().getId());
            }
            catch (ServiceException e)
            {
                friendRequestId = null;
            }

            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("userWindow.fxml"));

            userWindowController controller = new userWindowController();
            controller.setService(this.service);
            DTOFriendship user = tableFriendList.getSelectionModel().getSelectedItem();
            String email = null;
            for (DTOUser serviceUser : service.getUsers(this.currentUser.getID()))
            {
                if(serviceUser.getID().equals(user.getId()))
                {
                    email = serviceUser.getEmail();
                }
            }
            controller.setUsers(this.currentUser , new DTOUser(user.getId() , email , user.getFirst_name() , user.getLast_name()) , friendRequestId);
            controller.setParentController(this);
            loader.setController(controller);
            try
            {
                newStage.setScene(new Scene(loader.load()));
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            newStage.show();
        });
        this.tableFriendRequest.setOnMouseClicked(x->
        {
            Long id = null;
            if(this.tableFriendRequest.getSelectionModel().getSelectedItem().getFrom_ID() == this.currentUser.getID())
            {
                id = this.tableFriendRequest.getSelectionModel().getSelectedItem().getTo_ID();
            }
            else
            {
                id = this.tableFriendRequest.getSelectionModel().getSelectedItem().getFrom_ID();
            }
            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("friendRequestWindow.fxml"));

            friendRequestWindowController controller = new friendRequestWindowController();
            controller.setService(this.service);
            controller.setUsers(currentUser , this.tableFriendRequest.getSelectionModel().getSelectedItem());
            loader.setController(controller);
            try {
                newStage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            newStage.show();
        });
    }

    @Override
    public void update(AbstractEvent abstractEvent)
    {
        if(abstractEvent.getAffected() == AffectedByEvent.FRIENDREQUEST)
        {
            handleFriendRequestUpdate(abstractEvent.getType() , (DTOFriendRequest) abstractEvent.getData() , (DTOFriendRequest) abstractEvent.getOldData());
        }
        if(abstractEvent.getAffected() == AffectedByEvent.FRIENDSHIP)
        {
            handleFriendshipUpdate(abstractEvent.getType() , (DTOFriendship) abstractEvent.getData() , (DTOFriendship) abstractEvent.getOldData());
        }
        if(abstractEvent.getAffected() == AffectedByEvent.USER)
        {
            handleUserUpdate(abstractEvent.getType() , (DTOUser) abstractEvent.getData() , (DTOUser) abstractEvent.getOldData());
        }
    }

    private void handleFriendRequestUpdate(ChangeEventType type, DTOFriendRequest data, DTOFriendRequest oldData)
    {
        if(type == ChangeEventType.ADD)
        {
            modelRequest.add(data);
        }
        if(type == ChangeEventType.DELETE)
        {
            int index = 0;
            int saved_index = 0;
            for (DTOFriendRequest friendRequest : modelRequest)
            {
                if(friendRequest.equals(data))
                {
                    saved_index = index;
                }
                index++;
            }
            modelRequest.remove(saved_index);
        }
        if(type == ChangeEventType.UPDATE)
        {
            int saved_index = 0;
            for (int i = 0; i < modelRequest.size(); i++)
            {
                if(modelRequest.get(i).equals(data))
                {
                    saved_index = i;
                }
            }
            modelRequest.remove(saved_index);
            modelRequest.add(saved_index , data);
        }
    }

    private void handleUserUpdate(ChangeEventType type , DTOUser data , DTOUser oldData)
    {
        if(type == ChangeEventType.ADD)
        {
            modelUser.add(data);
        }
        if(type == ChangeEventType.DELETE)
        {

        }
        if(type == ChangeEventType.UPDATE)
        {

        }
    }

    private void handleFriendshipUpdate(ChangeEventType type, DTOFriendship data, DTOFriendship oldData)
    {
        if(type == ChangeEventType.ADD)
        {
            modelGrade.add(data);
        }
        if(type == ChangeEventType.DELETE)
        {
            int index = 0;
            int saved_index = 0;
            for (DTOFriendship dtoFriendship : modelGrade)
            {
                if(modelGrade.get(index).equals(data))
                {
                    saved_index = index;
                }
                index++;
            }
            modelGrade.remove(saved_index);
        }
    }

}
