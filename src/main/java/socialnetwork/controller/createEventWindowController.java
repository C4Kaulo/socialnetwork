package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.UtilizatorService;

import java.io.File;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

public class createEventWindowController implements Initializable
{
    String filePath;
    Stage currentStage;
    DTOUser user;
    UtilizatorService service;

    @FXML
    Button createQuick;

    @FXML
    DatePicker datePicker;

    @FXML
    StackPane imageStackPane;

    @FXML
    Button chooseBackgroundButton;

    @FXML
    TextField titleText;

    @FXML
    TextArea descriptionText;

    @FXML
    Button createEvent;

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        this.chooseBackgroundButton.setOnMouseClicked(x->
        {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(currentStage);
            if (file != null)
            {
                filePath = file.toURI().toString();
                Image image1 = new Image(filePath);
                ImageView ip = new ImageView(image1);
                BackgroundSize backgroundSize = new BackgroundSize(100, 100, true, true, true, false);
                BackgroundImage backgroundImage = new BackgroundImage(image1, BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize);
                this.imageStackPane.setBackground(new Background(backgroundImage));
            }
        });
        this.createQuick.setOnMouseClicked(x->
        {
            try {
                this.service.createEvent(new LiveEvent(0L , titleText.getText() , descriptionText.getText(), LocalDateTime.now().plusMinutes(5),filePath ));
                this.currentStage.close();
            } catch (ServiceException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
        this.createEvent.setOnMouseClicked(x->
        {
            try {
                this.service.createEvent(new LiveEvent(0L , titleText.getText() , descriptionText.getText(),datePicker.getValue().atStartOfDay() ,filePath ));
                this.currentStage.close();
            } catch (ServiceException e) {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });

    }

    public void setPrerequisites(UtilizatorService service, DTOUser currentUser, Stage newStage)
    {
        this.service = service;
        this.currentStage = newStage;
        this.user = currentUser;
    }
}
