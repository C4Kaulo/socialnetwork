package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.service.UtilizatorService;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class notificationWindow implements Initializable
{
    DTOUser user;
    UtilizatorService service;

    @FXML
    ObservableList<EventNotification> notifications = FXCollections.observableArrayList();

    @FXML
    ListView<EventNotification> listViewNotifications;

    public void setPrerequisites(UtilizatorService service , DTOUser user)
    {
        this.service = service;
        this.user = user;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        Iterable<EventNotification> notif = service.getNotifications(this.user.getID());
        notifications.setAll(
                StreamSupport.stream(notif.spliterator(), false)
                        .collect(Collectors.toList()));
        listViewNotifications.setItems(notifications);
        notif.forEach(x-> service.userNotified(x));
    }
}
