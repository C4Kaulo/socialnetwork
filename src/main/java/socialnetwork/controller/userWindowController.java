package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import socialnetwork.domain.dto.DTOFriendRequest;
import socialnetwork.domain.dto.DTOFriendship;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class userWindowController implements Initializable , Observer<AbstractEvent>
{
    Long friendshipId;
    DTOUser user;
    DTOUser otherUser;
    UtilizatorService service;
    mainMenuController parentController;

    @FXML
    Label emailLabel;

    @FXML
    Label usernameLabel;

    @FXML
    HBox hboxButtons;

    @FXML
    Button addUserButton;

    @FXML
    Button cancelFriendRequest;

    @FXML
    Button removeUserButton;

    @FXML
    Button openChatButton;

    public void setService(UtilizatorService service)
    {
        this.service = service;
        this.service.addObserver(this);
    }

    public void setUsers(DTOUser user , DTOUser otherUser , Long friendshipId)
    {
        this.friendshipId = friendshipId;
        this.user = user;
        this.otherUser = otherUser;
    }
    public void setParentController(mainMenuController parentController)
    {
        this.parentController = parentController;
    }

    private void handleButtons()
    {
        hboxButtons.getChildren().removeAll();
        if(service.isFriend(user.getID() , otherUser.getID()))
        {
            hboxButtons.getChildren().add(openChatButton);
            hboxButtons.getChildren().add(removeUserButton);
        }
        else
        {
            hboxButtons.getChildren().add(openChatButton);
            hboxButtons.getChildren().add(addUserButton);
            if (service.hasPendingRequest(user.getID() , otherUser.getID()))
            {
                hboxButtons.getChildren().add(cancelFriendRequest);
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        System.out.println(this.friendshipId);
        //1)PENDING\n2)APPROVED\n3)REJECTED
        emailLabel.setText(otherUser.getEmail());
        usernameLabel.setText(otherUser.getFirstName() + " " + otherUser.getLastName());

        handleButtons();
        addUserButton.setOnMouseClicked(x->
        {
            try
            {
                this.service.sendFriendRequest(user.getID() , otherUser.getID());
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
        removeUserButton.setOnMouseClicked(x->
        {
            try
            {
                System.out.println("REMOVE HAS BEEN CLICKED!");
                this.service.removeFriendUtilizator(user.getID() , otherUser.getID());
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
        cancelFriendRequest.setOnMouseClicked(x->
        {
            try
            {
                this.service.cancelFriendRequest(this.friendshipId , this.user.getID());
                //this.service.handleFriendRequest(this.friendshipId , this.user.getID() , "REJECTED");
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });

        openChatButton.setOnMouseClicked(x->
        {
            Stage newStage = new Stage();
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("chatBox.fxml"));

            chatBoxController controller = new chatBoxController();
            controller.setService(this.service);
            ArrayList<DTOUser> allusers = new ArrayList<>();
            allusers.add(this.user);
            allusers.add(this.otherUser);
            controller.setAllUsers(allusers);
            loader.setController(controller);
            controller.setCurrentUser(this.user);

            try {
                newStage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            newStage.show();
        });

    }

    @Override
    public void update(AbstractEvent abstractEvent)
    {
        if(abstractEvent.getAffected() == AffectedByEvent.FRIENDREQUEST)
        {
            handleFriendRequestUpdate(abstractEvent.getType() , (DTOFriendRequest) abstractEvent.getData() , (DTOFriendRequest) abstractEvent.getOldData());
        }
        if(abstractEvent.getAffected() == AffectedByEvent.FRIENDSHIP)
        {
            handleFriendshipUpdate(abstractEvent.getType() , (DTOFriendship) abstractEvent.getData() , (DTOFriendship) abstractEvent.getOldData());
        }
    }

    private void handleFriendshipUpdate(ChangeEventType type, DTOFriendship data, DTOFriendship oldData)
    {
        if(type == ChangeEventType.ADD)
        {
            /*@FXML
            Button addUserButton;

            @FXML
            Button cancelFriendRequest;

            @FXML
            Button removeUserButton;

            @FXML
            Button openChatButton;*/
            hboxButtons.getChildren().clear();
            hboxButtons.getChildren().addAll(openChatButton , removeUserButton);
        }
        if(type == ChangeEventType.DELETE)
        {
            hboxButtons.getChildren().clear();
            hboxButtons.getChildren().addAll(openChatButton , addUserButton);
        }
    }

    private void handleFriendRequestUpdate(ChangeEventType type, DTOFriendRequest data, DTOFriendRequest oldData)
    {
        if( (data.getFrom_ID() == user.getID() || data.getTo_ID() == user.getID()) &&
                (data.getTo_ID() == otherUser.getID() || data.getFrom_ID() == otherUser.getID()) )
        {
            if(type == ChangeEventType.ADD)
            {
                hboxButtons.getChildren().clear();
                hboxButtons.getChildren().addAll(openChatButton , cancelFriendRequest);
                try {
                    friendshipId = service.getFriendRequestId(user.getID() , otherUser.getID());
                } catch (ServiceException e) {
                    System.out.println(e.getErrorMessage());
                }
            }
            if(type == ChangeEventType.DELETE)
            {
                hboxButtons.getChildren().clear();
                hboxButtons.getChildren().addAll(openChatButton , addUserButton);
            }
            if(type == ChangeEventType.UPDATE)
            {
                if(data.getStatus().equals("REJECTED"))
                {
                    hboxButtons.getChildren().clear();
                    hboxButtons.getChildren().addAll(openChatButton , addUserButton);
                }
                if(data.getStatus().equals("APPROVED"))
                {
                    hboxButtons.getChildren().clear();
                    hboxButtons.getChildren().addAll(openChatButton , removeUserButton);
                }
                friendshipId = null;
            }
        }
    }
}
