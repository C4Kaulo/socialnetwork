package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.service.UtilizatorService;

import java.net.URL;
import java.util.ResourceBundle;

public class activityPdfGeneratorController implements Initializable
{
    DTOUser user;
    UtilizatorService service;

    @FXML
    DatePicker fromDate;

    @FXML
    DatePicker toDate;

    @FXML
    Button button;

    public void setPrerequisites(UtilizatorService service , DTOUser user )
    {
        this.service = service;
        this.user = user;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        button.setOnMouseClicked(x->
        {
            service.generatePDFActivity(this.user.getID() , fromDate.getValue().atStartOfDay() , toDate.getValue().atStartOfDay());
        });
    }
}
