package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import socialnetwork.domain.dto.DTOFriendRequest;
import socialnetwork.domain.dto.DTOFriendship;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.observer.Observer;

import java.net.URL;
import java.util.ResourceBundle;

public class friendRequestWindowController implements Initializable , Observer<AbstractEvent>
{

    final String arrow = "file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/arrow.jpg";
    final String checkmark = "file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/checkmark.jpg";
    final String rejected = "file:/D:/Anul2/MAP/Laborator/Lab4-5/src/main/resources/images/rejected.jpg";
    DTOUser user;
    DTOUser otherUser;
    boolean toUser;
    DTOFriendRequest friendRequest;
    UtilizatorService service;

    @FXML
    ImageView statusImage;

    @FXML
    TextField fromTextField;

    @FXML
    TextField toTextField;

    @FXML
    Label statusLabel;

    @FXML
    HBox buttonsBox;

    @FXML
    Button rejectFriendRequestButton;

    @FXML
    Button cancelFriendRequestButton;

    @FXML
    Button acceptFriendRequestButton;


    public void setService(UtilizatorService service)
    {
        this.service = service;
        this.service.addObserver(this);
    }

    public void setUsers(DTOUser user , DTOFriendRequest friendRequest)
    {
        this.user = user;
        this.friendRequest = friendRequest;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        if(this.user.getID().equals(friendRequest.getTo_ID()))
        {
            toUser = true;
            otherUser = service.getOneUser(friendRequest.getFrom_ID());
            fromTextField.setText(otherUser.getFirstName() +" "+ otherUser.getLastName());
            toTextField.setText(user.getFirstName() + " " + user.getLastName());
        }
        else
        {
            toUser = false;
            otherUser = service.getOneUser(friendRequest.getTo_ID());
            fromTextField.setText(user.getFirstName() +" "+ user.getLastName());
            toTextField.setText(otherUser.getFirstName() + " " + otherUser.getLastName());
        }

        if (friendRequest.getStatus().equals("PENDING"))
        {
            statusImage.setImage(new Image(arrow));
            statusLabel.setText("PENDING");
            if(toUser)
            {
                buttonsBox.getChildren().addAll(acceptFriendRequestButton , rejectFriendRequestButton);
            }
            else
            {
                buttonsBox.getChildren().addAll(cancelFriendRequestButton);
            }

        }
        if (friendRequest.getStatus().equals("APPROVED"))
        {
            statusImage.setImage(new Image(checkmark));
            statusLabel.setText("APPROVED");
        }
        if (friendRequest.getStatus().equals("REJECTED"))
        {
            statusImage.setImage(new Image(rejected));
            statusLabel.setText("REJECTED");
        }

        rejectFriendRequestButton.setOnMouseClicked(x->
        {
            try
            {
                this.service.handleFriendRequest(this.friendRequest.getID() , this.user.getID() , "REJECTED");
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });

        cancelFriendRequestButton.setOnMouseClicked(x->
        {
            try
            {
                this.service.cancelFriendRequest(this.friendRequest.getID() , this.user.getID());
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
        acceptFriendRequestButton.setOnMouseClicked(x->
        {
            try
            {
                this.service.handleFriendRequest(this.friendRequest.getID() , this.user.getID() , "APPROVED");
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
    }

    @Override
    public void update(AbstractEvent abstractEvent)
    {
        if(abstractEvent.getAffected() == AffectedByEvent.FRIENDREQUEST)
        {
            handleFriendRequestUpdate(abstractEvent.getType() , (DTOFriendRequest) abstractEvent.getData() , (DTOFriendRequest) abstractEvent.getOldData());
        }
    }

    private void handleFriendRequestUpdate(ChangeEventType type, DTOFriendRequest data, DTOFriendRequest oldData)
    {
        if(type == ChangeEventType.DELETE)
        {
            statusImage.setImage(new Image(rejected));
            statusLabel.setText("CANCELLED");
            buttonsBox.getChildren().clear();
        }
        if(type == ChangeEventType.UPDATE)
        {
            if (data.getStatus().equals("APPROVED"))
            {
                statusImage.setImage(new Image(checkmark));
                statusLabel.setText("APPROVED");
                buttonsBox.getChildren().clear();
            }
            if (data.getStatus().equals("REJECTED"))
            {
                statusImage.setImage(new Image(rejected));
                statusLabel.setText("REJECTED");
                buttonsBox.getChildren().clear();
            }
        }
    }


}
