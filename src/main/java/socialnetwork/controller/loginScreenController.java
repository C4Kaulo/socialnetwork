package socialnetwork.controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseDragEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import jdk.internal.loader.Loader;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.notifications.Notifier;
import sun.nio.ch.Util;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class loginScreenController implements Initializable
{
    private Stage primaryStage;
    private UtilizatorService service;

    @FXML
    TextField username;

    @FXML
    TextField password;

    @FXML
    Button signUpButton;

    @FXML
    Button loginButton;

    @FXML
    Label errorLabel;

    public void setService(UtilizatorService service)
    {
        this.service = service;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        errorLabel.setVisible(false);
    }

    public void start()
    {
        this.primaryStage.show();
    }

    public void login_onMouseClicked()
    {
        System.out.println(username.getText() + "\n" + password.getText());
        Utilizator user = service.getLoginRequest(username.getText() , password.getText());
        if (user == null)
        {
            errorLabel.setVisible(true);
            errorLabel.setText("Invalid username or password");
        }
        else
        {
            FXMLLoader loader = null;
            loader = new FXMLLoader(getClass().getClassLoader().getResource("mainMenu.fxml"));

            mainMenuController controller = new mainMenuController();
            controller.setService(this.service);
            Notifier notifier = new Notifier(this.service , new DTOUser(user.getEntity() , user.getEmail() , user.getFirstName() , user.getLastName()));
            Thread th = new Thread(notifier);
            th.start();
            loader.setController(controller);
            controller.setCurrentUser(new DTOUser(user.getEntity() , user.getEmail() , user.getFirstName() , user.getLastName()));
            try {
                this.primaryStage.setScene(new Scene(loader.load()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void signUp_onMouseClicked()
    {
        Stage stage = new Stage();
        FXMLLoader loader = null;
        loader = new FXMLLoader(getClass().getClassLoader().getResource("signUp.fxml"));

        signUpController controller = new signUpController();
        controller.setService(this.service);
        controller.setStage(stage);
        loader.setController(controller);
        try {
            stage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        stage.show();
    }

    public void setPrimaryStage(Stage primaryStage)
    {
        this.primaryStage = primaryStage;
    }
}
