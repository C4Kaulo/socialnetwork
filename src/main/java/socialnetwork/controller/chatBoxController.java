package socialnetwork.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.DTOConversation;
import socialnetwork.domain.dto.DTOMessage;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.messages.Message;
import socialnetwork.service.UtilizatorService;

import java.io.FileOutputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class chatBoxController implements Initializable
{
    UtilizatorService service;
    DTOUser currentUser;
    ArrayList<DTOUser> users;
    Message reply = null;

    ObservableList<DTOConversation> listView = FXCollections.observableArrayList();
    ObservableList<DTOUser> userView = FXCollections.observableArrayList();

    @FXML
    ListView<DTOUser> presentUsersList;

    @FXML
    ListView<DTOConversation> conversationListView;//Chat box\\

    @FXML
    TextField textField;//Input message\\

    @FXML
    Button ButtonSend;

    @FXML
    HBox replierHbox;

    @FXML
    Label replyingToMessage = new Label();

    @FXML
    ImageView cancelImageView;

    @FXML
    DatePicker fromDate;

    @FXML
    Label replyLabel;

    @FXML
    DatePicker toDate;

    @FXML
    Button generate;

    class customCell extends ListCell<DTOConversation>
    {
        VBox layout = new VBox();
        HBox top = new HBox();
        Label username = new Label();
        Label date = new Label();
        Label reply2 = new Label();
        Label content = new Label();
        HBox bottom = new HBox();
        Button button = new Button("Reply");

        public customCell()
        {
            super();
            button.setOnMouseClicked(x->
            {
                reply = this.getItem().getMessage();
                replyingToMessage.setText(this.content.getText());
                replierHbox.getChildren().addAll(replyLabel , replyingToMessage , cancelImageView);
            });
            top.getChildren().addAll(username , date , reply2 , button);
            bottom.getChildren().addAll(content);
            layout.getChildren().addAll(top , bottom);
        }

        @Override
        protected void updateItem(DTOConversation item, boolean empty)
        {
            super.updateItem(item, empty);

            if (item == null)
            {
                return;
            }
            if(item.getRepliedMessage() == null)
            {
                username.setText(item.getUser().getFirstName() + " " + item.getUser().getLastName() + " ");
                date.setText( "  "+ item.getMessage().getDate().getHour() + ":" + item.getMessage().getDate().getMinute() + "   ");
                content.setText(item.getMessage().getContent());
            }
            else
            {
                username.setText(item.getUser().getFirstName() + " " + item.getUser().getLastName() + " ");
                date.setText( "  "+ item.getMessage().getDate().getHour() + ":" + item.getMessage().getDate().getMinute() + "   ");
                content.setText(item.getMessage().getContent() +"\n[REPLY TO]:   "+ item.getRepliedMessage().getMessage().getContent());
            }
            setGraphic(layout);
        }
    }

    private void loadTextArea()
    {
        listView.clear();
        for (DTOConversation dtoConversation : this.service.getAllConversation(this.users))
        {
            listView.add(dtoConversation);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        cancelImageView.setOnMouseClicked(x->
        {
            this.reply = null;
            replierHbox.getChildren().clear();
        });
        userView.addAll(users);
        conversationListView.setItems(listView);
        conversationListView.setCellFactory(param -> new customCell());
        presentUsersList.setItems(userView);
        loadTextArea();
        ButtonSend.setOnMouseClicked(x->
        {
            ArrayList<Long> to = new ArrayList<>();
            for (DTOUser user : users) {
                if(!user.getID().equals(currentUser.getID()))
                {
                    to.add(user.getID());
                }
            }
            if(reply == null)
                this.service.createMessage(currentUser.getID() , to , textField.getText());
            else this.service.createMessage(currentUser.getID() , to , textField.getText(), reply.getEntity());
            loadTextArea();
        });

        generate.setOnMouseClicked(x->
        {
            try {
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream("D:\\Anul2\\MAP\\Laborator\\Lab4-5\\chat.pdf"));
                document.open();
                document.add(new Paragraph("Messages sent : \n"));
                for (DTOConversation dtoConversation : this.service.getAllConversation(this.users)) {
                    if(dtoConversation.getRepliedMessage() == null)
                    {
                        document.add(new Paragraph("["+dtoConversation.getMessage().getDate() + "] " +
                                dtoConversation.getUser().getFirstName() + dtoConversation.getUser().getLastName() + " : " +
                                dtoConversation.getMessage().getContent()));
                    }
                    else
                    {
                        document.add(new Paragraph("["+dtoConversation.getMessage().getDate() + "] " +
                                dtoConversation.getUser().getFirstName() + dtoConversation.getUser().getLastName() + " : " +
                                dtoConversation.getMessage().getContent() + "    " + dtoConversation.getRepliedMessage().getMessage().getContent()));
                    }
                }
                document.close();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        });
    }

    public void setService(UtilizatorService service)
    {
        this.service = service;
    }

    public void setAllUsers(ArrayList<DTOUser> list)
    {
        this.users = list;
    }

    public void setCurrentUser(DTOUser user)
    {
        this.currentUser = user;
    }
}
