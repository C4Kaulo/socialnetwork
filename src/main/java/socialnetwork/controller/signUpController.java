package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.service.UtilizatorService;

import java.net.URL;
import java.util.ResourceBundle;

public class signUpController implements Initializable
{
    Stage stage;
    UtilizatorService service;

    @FXML
    Button signUpButton;

    @FXML
    TextField textFieldEmail;

    @FXML
    TextField textFieldFirstName;

    @FXML
    TextField textFieldLastName;

    @FXML
    PasswordField passwordFieldPassword;

    public void setService(UtilizatorService service)
    {
        this.service = service;
    }

    public void setStage(Stage stage)
    {
        this.stage = stage;
    }

    public void start()
    {
        this.stage.show();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources)
    {
        signUpButton.setOnMouseClicked(x->
        {
            String email = textFieldEmail.getText();
            String firstName = textFieldFirstName.getText();
            String lastName = textFieldLastName.getText();
            String password = passwordFieldPassword.getText();
            try
            {
                service.getSignUpRequest(email , firstName , lastName ,password);
            }
            catch (ServiceException e)
            {
                Alert alert = new Alert(Alert.AlertType.ERROR , e.getErrorMessage() , ButtonType.OK);
                alert.showAndWait();
            }
        });
    }
}
