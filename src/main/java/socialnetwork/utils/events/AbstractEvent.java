package socialnetwork.utils.events;

public class AbstractEvent implements Event
{
    private AffectedByEvent affected;
    private ChangeEventType type;
    Object data , oldData;

    public AbstractEvent(ChangeEventType type , AffectedByEvent affected , Object data , Object oldData)
    {
        this.type = type;
        this.affected = affected;
        this.data = data;
        this.oldData = oldData;
    }

    public AbstractEvent(ChangeEventType type , AffectedByEvent affected , Object data)
    {
        this.type = type;
        this.affected = affected;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public Object getOldData() {
        return oldData;
    }

    public AffectedByEvent getAffected()
    {
        return affected;
    }

    public ChangeEventType getType()
    {
        return type;
    }

}
