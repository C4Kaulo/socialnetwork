package socialnetwork.utils.events;

public enum AffectedByEvent
{
    FRIENDSHIP,FRIENDREQUEST,USER,MESSAGE,EVENT
}
