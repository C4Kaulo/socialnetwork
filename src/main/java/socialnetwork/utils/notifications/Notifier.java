package socialnetwork.utils.notifications;

import jdk.vm.ci.meta.Local;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.observer.Observer;

import javax.tools.Tool;
import java.awt.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;

public class Notifier implements Runnable , Observer<AbstractEvent>
{
    DTOUser user;
    ArrayList<LiveEvent> events;
    ArrayList<EventNotification> connection;
    UtilizatorService service;

    public Notifier(UtilizatorService service , DTOUser user)
    {
        this.user = user;
        this.service = service;
        this.service.addObserver(this);
    }

    @Override
    public void run()
    {
        connection = this.service.getConnections(user.getID());
        events = this.service.getAllEventsForUser(user.getID());

        SystemTray tray = SystemTray.getSystemTray();
        Image image = Toolkit.getDefaultToolkit().createImage("icon.png");
        TrayIcon trayIcon = new TrayIcon(image , "This is a nice tray");
        trayIcon.setImageAutoSize(true);
        trayIcon.setToolTip("Merge merge");
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            e.printStackTrace();
        }
        while (true)
        {
            try {
                Thread.sleep(6 * 10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (LiveEvent event : events)
            {
                for (EventNotification eventNotification : connection)
                {
                    {
                        if(eventNotification.getNotified() && eventNotification.getId_event().equals(event.getId()))
                        {
                            if(LocalDateTime.now().isAfter(event.getDate()))
                            {
                                eventNotification.setNotified(false);
                                service.userNotified(eventNotification);
                                trayIcon.displayMessage("An event is coming up !" , "The event " + event.getTitle() + " is happening right now!" , TrayIcon.MessageType.INFO);
                            }
                            else
                            {
                                trayIcon.displayMessage("An event is coming up !" , "There are " + (event.getDate().getMinute() - LocalDateTime.now().getMinute()) + " minutes left until the event " + event.getTitle() ,TrayIcon.MessageType.INFO);
                            }
                        }
                    }
                }
            }

        }
    }

    @Override
    public void update(AbstractEvent abstractEvent)
    {
        if(abstractEvent.getAffected() == AffectedByEvent.EVENT)
        {
            handleUpdate(abstractEvent.getType() , (EventNotification)abstractEvent.getData() , (EventNotification)abstractEvent.getOldData());
        }
    }

    private void handleUpdate(ChangeEventType type ,EventNotification data , EventNotification oldData)
    {
        if(type == ChangeEventType.ADD)//Added event for user
        {
            //Cand dai subscribe
            events = this.service.getAllEventsForUser(user.getID());
            connection = this.service.getConnections(user.getID());
        }
        if(type == ChangeEventType.DELETE)//notification delete
        {
            int index = 0;
            //Cand dai turn off la notificatii
            for (EventNotification eventNotification : connection)
            {
                if(data.getId().equals(eventNotification.getId()))
                {
                    connection.set(index , data);
                }
                index++;
            }
        }
        if(type == ChangeEventType.UPDATE)//notification add
        {
            //Cand dai turn on la notificatii DUPA ce ai dat turn off
            int index = 0;
            //Cand dai turn off la notificatii
            for (EventNotification eventNotification : connection)
            {
                if(data.getId().equals(eventNotification.getId()))
                {
                    connection.set(index , data);
                }
                index++;
            }
        }
    }
}
