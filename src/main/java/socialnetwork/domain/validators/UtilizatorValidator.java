package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    IDValidator idValidator = new IDValidator();
    private void validateString(String toValidate) throws ValidationException
    {
        for(int i = 0; i < toValidate.length(); i++)
        {
            if (!(toValidate.charAt(i) >= 'a' && toValidate.charAt(i) <= 'z' ||
                    toValidate.charAt(i) >='A' && toValidate.charAt(i) <= 'Z'))
            {
                throw new ValidationException("Invalid string passed as name");
            }
        }
    }
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        idValidator.validate(entity.getEntity());
        validateString(entity.getFirstName());
        validateString(entity.getLastName());
    }
}
