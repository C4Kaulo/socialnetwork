package socialnetwork.domain.validators;

public class IDValidator implements Validator<Long>{
    @Override
    public void validate(Long entity) throws ValidationException {
        if (entity < 0)
        {
            throw new ValidationException("ID invalid");
        }
    }
}
