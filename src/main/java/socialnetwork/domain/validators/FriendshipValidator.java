package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;

public class FriendshipValidator implements Validator<Prietenie>
{
    private IDValidator idValidator = new IDValidator();
    @Override
    public void validate(Prietenie entity) throws ValidationException
    {
        idValidator.validate(entity.getId());
    }
}
