package socialnetwork.domain;

import jdk.vm.ci.meta.Local;

import java.time.LocalDateTime;


public class Prietenie extends Entity<Tuple<Long,Long>> {

    private LocalDateTime date;
    private Long id;

    public Prietenie(Long ID_student , Long ID_friend , LocalDateTime date_created)
    {
        super.setEntity(new Tuple<Long , Long>(ID_student , ID_friend));
        this.date = date_created;
    }
    public Long getId()
    {
        return this.id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate()
    {
        return date;
    }

    public boolean isEqual(Prietenie other)
    {
        return (other.getEntity().getRight().equals(this.getEntity().getLeft()) && other.getEntity().getLeft().equals(this.getEntity().getRight())) ||
                (other.getEntity().getRight().equals(this.getEntity().getRight()) && other.getEntity().getLeft().equals(this.getEntity().getLeft()));
    }
}
