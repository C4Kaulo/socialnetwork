package socialnetwork.domain.dto;

import socialnetwork.domain.messages.Message;

import java.time.LocalDateTime;

public class DTOConversation
{
    DTOUser user;
    Message message;
    DTOConversation repliedMessage = null;

    public DTOConversation(DTOUser user , Message message)
    {
        this.user = user;
        this.message = message;
    }

    public DTOConversation(DTOUser user , Message message , DTOConversation repliedMessage)
    {
        this.user = user;
        this.message = message;
        this.repliedMessage = repliedMessage;
    }

    public DTOConversation getRepliedMessage() {
        return repliedMessage;
    }

    public DTOUser getUser() {
        return user;
    }

    public Message getMessage() {
        return message;
    }

}
