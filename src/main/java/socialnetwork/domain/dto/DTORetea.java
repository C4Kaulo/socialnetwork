package socialnetwork.domain.dto;
import socialnetwork.domain.Utilizator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class DTORetea implements Serializable {
    Vector<Vector<Utilizator>> list;

    public DTORetea(Vector<Vector<Utilizator>> res)
    {
        this.list = res;
    }

    @Override
    public String toString()
    {
        String res = new String();
        int nr_com = 1;
        for(Vector<Utilizator> i : list)
        {
            res+= "Comunitatea" + nr_com +":\n";
            for(Utilizator j : i)
            {
                res += j.getFirstName() + " ";
            }
            res += "\n";
            nr_com++;
        }
        return res;
    }
}
