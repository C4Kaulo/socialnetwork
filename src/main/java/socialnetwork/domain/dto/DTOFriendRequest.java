package socialnetwork.domain.dto;

public class DTOFriendRequest
{

    private Long ID;
    private Long From_ID;
    private Long To_ID;
    private String Nume;
    private String Status;

    public DTOFriendRequest (Long ID,Long From_ID,Long To_ID, String Nume, String Status)
    {
        this.ID=ID;
        this.To_ID=To_ID;
        this.From_ID=From_ID;
        this.Nume=Nume;
        this.Status=Status;
    }
    public DTOFriendRequest(DTOFriendRequest other)
    {
        this.ID=        other.ID;
        this.To_ID=     other.To_ID;
        this.From_ID=   other.From_ID;
        this.Nume=      other.Nume;
        this.Status=    other.Status;
    }

    public void setStatus(String otherStatus)
    {
        this.Status = otherStatus;
    }

    public Long getTo_ID()
    {
        return To_ID;
    }

    public Long getID()
    {
        return ID;
    }

    public Long getFrom_ID()
    {
        return From_ID;
    }

    public String getNume()
    {
        return Nume;
    }

    public String getStatus()
    {
        return Status;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        DTOFriendRequest other = (DTOFriendRequest) obj;
        return this.ID.equals(other.getID());
    }
}
