package socialnetwork.domain.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

public class DTOFriendship implements Serializable
{
    private String first_name;
    private String last_name;
    private LocalDateTime date;
    private Long id;
    public DTOFriendship(Long id , String first_name , String last_name , LocalDateTime date)
    {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.date = date;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String toString()
    {
        return "" + this.first_name + "|" + this.last_name + "|" + this.date;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        DTOFriendship other = (DTOFriendship) obj;
        return (this.id.equals(other.getId()));
    }
}
