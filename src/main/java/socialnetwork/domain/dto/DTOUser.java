package socialnetwork.domain.dto;

import socialnetwork.domain.Utilizator;

import java.util.ArrayList;
import java.util.List;

public class DTOUser
{
    private String email;
    private Long ID;
    private String firstName;
    private String lastName;

    public DTOUser(Long ID ,String email, String firstName, String lastName)
    {
        this.email = email;
        this.ID=ID;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public Long getID() {
        return ID;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        if(this== null || obj == null)
            return false;
        DTOUser other = (DTOUser) obj;
        return (this.ID.equals(other.getID()));
    }

    @Override
    public String toString() {
        return this.firstName + " " + this.lastName;
    }
}
