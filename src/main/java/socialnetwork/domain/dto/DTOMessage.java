package socialnetwork.domain.dto;

public class DTOMessage
{
    private Long id;
    private String name;
    private Long withUser;
    public DTOMessage(Long id , Long withUser , String name)
    {
        this.name = name;
        this.id = id;
        this.withUser = withUser;
    }

    public String getName()
    {
        return name;
    }

    public Long getId()
    {
        return id;
    }

    public Long getWithUser()
    {
        return withUser;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this == obj)
        {
            return true;
        }
        DTOMessage other = (DTOMessage) obj;
        return (this.id.equals(other.getId()));
    }
}
