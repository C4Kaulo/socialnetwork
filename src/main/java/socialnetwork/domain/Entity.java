package socialnetwork.domain;

import java.io.Serializable;

public class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 7331115341259248461L;
    private ID entity;
    public ID getEntity() {
        return entity;
    }
    public void setEntity(ID id) {
        this.entity = id;
    }

}