package socialnetwork.domain;

public class Password extends Entity<Long>
{
    private Long id_user;
    private String password;
    private String salt;

    public Password(Long id_user ,String salt, String password)
    {
        this.salt = salt;
        this.id_user = id_user;
        this.password = password;
    }

    public String getSalt()
    {
        return salt;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
