package socialnetwork.domain.liveevents;

import socialnetwork.domain.Entity;

public class EventNotification extends Entity<Long>
{
    Long id;
    Long id_event;
    Long id_user;
    Boolean notified;
    LiveEvent event;

    public EventNotification(Long id , Long id_user , Long id_event , Boolean notified)
    {
        this.id = id;
        this.id_user = id_user;
        this.id_event = id_event;
        this.notified = notified;
    }

    public void setEvent(LiveEvent event) {
        this.event = event;
    }

    public LiveEvent getEvent()
    {
        return this.event;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId_event() {
        return id_event;
    }

    public void setId_event(Long id_event) {
        this.id_event = id_event;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public Boolean getNotified() {
        return notified;
    }

    public void setNotified(Boolean notified) {
        this.notified = notified;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj)
            return true;
        if (this == null || obj == null)
            return false;
        EventNotification other = (EventNotification) obj;
        return (this.id.equals(other.getId()));
    }

    @Override
    public String toString() {
        return "Notificare despre evenimentul cu id-ul " + this.id_event;
    }
}
