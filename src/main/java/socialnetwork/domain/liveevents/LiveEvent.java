package socialnetwork.domain.liveevents;

import socialnetwork.domain.Entity;
import socialnetwork.domain.dto.DTOUser;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LiveEvent extends Entity<Long>
{
    private Long id;
    private String fileLocation;
    private String title;
    private String description;
    private LocalDateTime date;
    private List<Long> idSubscribedUsers;
    private List<DTOUser> subscribedUsers;

    public LiveEvent(Long id , String title, String description, LocalDateTime date ,String fileLocation)
    {
        this.id = id;
        this.title = title;
        this.description = description;
        this.date = date;
        this.subscribedUsers = new ArrayList<>();
        this.fileLocation = fileLocation;
    }

    public void addUser(DTOUser user)
    {
        this.subscribedUsers.add(user);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Long> getIdSubscribedUsers() {
        return idSubscribedUsers;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public List<DTOUser> getSubscribedUsers() {
        return subscribedUsers;
    }
}
