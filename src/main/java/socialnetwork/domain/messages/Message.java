package socialnetwork.domain.messages;
import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Message extends Entity<Long>
{
    private Long originalMessageId;
    private Long from;
    private LocalDateTime date;
    private String content;

    public Message(Long from, LocalDateTime dateTime , String content , Long originalMessageId)
    {
        this.from = from;
        this.originalMessageId = originalMessageId;
        this.date = dateTime;
        this.content = content;
    }
    public void setID(Long id)
    {
        super.setEntity(id);
    }

    public LocalDateTime getDate()
    {
        return date;
    }

    public Long getFrom()
    {
        return from;
    }

    public Long getOriginalMessageId()
    {
        return originalMessageId;
    }

    public String getContent()
    {
        return content;
    }

    public boolean isEqual(Message other)
    {
        return this.originalMessageId.equals(other.getOriginalMessageId()) &&
                this.from.equals(other.getFrom()) &&
                this.date.equals(other.getDate()) &&
                this.content.equals(other.getContent());
    }

}
