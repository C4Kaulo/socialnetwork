package socialnetwork.domain.requests;

import socialnetwork.domain.Entity;

public class FriendRequest extends Entity<Long>
{
    private Long from;
    private Long to;
    String status ;
    public FriendRequest(Long from , Long to , String status)
    {
        this.from =  from;
        this.to = to;
        this.status = status;
    }

    public Long getFrom() {
        return from;
    }

    public Long getTo() {
        return to;
    }

    public String getStatus() {
        return status;
    }

    public boolean isEqual(FriendRequest other)
    {
        return other.getTo().equals(this.getTo()) && other.getFrom().equals(this.getFrom()) && other.getStatus().equals(this.getStatus());
    }

}
