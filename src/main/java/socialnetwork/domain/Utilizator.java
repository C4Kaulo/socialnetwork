package socialnetwork.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Utilizator extends Entity<Long>{
    private String email;
    private String firstName;
    private String lastName;
    private List<Utilizator> friends;

    public Utilizator(String email , String firstName, String lastName)
    {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = new ArrayList<Utilizator>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail()
    {
        return email;
    }

    public List<Utilizator> getFriends() {
        return friends;
    }

    public Utilizator addFriend(Utilizator friend)
    {
        this.friends.add(friend);
        return friend;
    }

    public Utilizator removeFriend(Utilizator friend)
    {
        this.friends.remove(friend);
        return friend;
    }

    @Override
    public String toString() {
        String format =  "Utilizator{" +
                "ID=" + this.getEntity() + " " +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", friends=";
        for (Utilizator friend : friends) {
            format = format + "'" + friend.getFirstName() + "' ";
        }
        format += '}';
        return format;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getFriends().equals(that.getFriends());
    }

    public boolean isEqual(Utilizator other)
    {
        return getFirstName().equals(other.getFirstName()) &&
                getLastName().equals(other.getLastName()) &&
                getFriends().equals(other.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getEntity());
    }
}