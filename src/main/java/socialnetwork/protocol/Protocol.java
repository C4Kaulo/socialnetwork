package socialnetwork.protocol;

import socialnetwork.domain.Entity;

import java.io.Serializable;

public interface Protocol<T>
{
    /*
    * @
    *
    *
    */
    void sendProtocol(T element);

    T receiveProtocol();
}
