package socialnetwork.exceptions;

public class ServiceException extends Throwable {
    private String errorMessage;

    public ServiceException(String errorMessage)
    {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage()
    {
        return this.errorMessage;
    }
}
