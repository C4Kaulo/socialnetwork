package socialnetwork;
import java.lang.reflect.Field;

public class Main implements Runnable{
    @Override
    public void run() {
        main(new String[]{"abc", "abc", "abc"});
    }

    class parent
    {
        Long parent_variable;
        protected parent()
        {
            this.parent_variable = 1L;
        }
    }
    class child extends parent
    {
        Long child_variable;
        protected child()
        {
            this.parent_variable = 2L;
            this.child_variable = 2L;
        }
    }

    public void test_class(parent a)
    {
        try {
            Field f = a.getClass().getDeclaredField("child_variable");
            f.setAccessible(true);
            System.out.println((Long) f.get(a));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void Test()
    {
        test_class(new child());
    }

    public static void main(String[] args) {
        /*Notifier not = new Notifier();
        Thread th = new Thread(not);
        th.start();*/
        //TestingArea.main(args);
        MainApp.main(args);
    }
}


