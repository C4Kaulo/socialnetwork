package socialnetwork.repository.paging;

import java.util.ArrayList;
import java.util.stream.Stream;

public interface Page<E>
{
    Pageable getPageable();

    Pageable nextPageable();

    ArrayList<E> getContent();
}
