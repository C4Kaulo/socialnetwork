package socialnetwork.repository.paging;

import java.util.ArrayList;
import java.util.stream.Stream;

public class PageImplementation<T> implements Page<T> {
    private Pageable pageable;
    private ArrayList<T> content;

    public PageImplementation(Pageable pageable, ArrayList<T> content) {
        this.pageable = pageable;
        this.content = content;
    }

    @Override
    public Pageable getPageable() {
        return this.pageable;
    }

    @Override
    public Pageable nextPageable() {
        return new PageableImplementation(this.pageable.getPageNumber() + 1, this.pageable.getPageSize());
    }

    @Override
    public ArrayList<T> getContent() {
        return this.content;
    }
}
