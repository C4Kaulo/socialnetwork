package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.PageImplementation;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

public class DbLiveEventRepository implements PagingRepository<Long , LiveEvent>
{
    private String url;
    private String username;
    private String password;

    public DbLiveEventRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public Page<LiveEvent> findAll(Pageable pageable)
    {
        ArrayList<LiveEvent> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM live_events ORDER BY id ASC OFFSET " + pageable.getPageNumber() * pageable.getPageSize() + " FETCH FIRST " + pageable.getPageSize() + " ROWS ONLY ");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                LiveEvent event = new LiveEvent(
                        resultSet.getLong("id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getTimestamp("date").toLocalDateTime(),
                        resultSet.getString("file_location")
                );
                res.add(event);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return new PageImplementation<>(pageable , res);
    }

    @Override
    public LiveEvent findOne(Long aLong) {
        LiveEvent entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM live_events WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                LiveEvent event = new LiveEvent(
                        resultSet.getLong("id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getTimestamp("date").toLocalDateTime(),
                        resultSet.getString("file_location")
                        );
                entity = event;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<LiveEvent> findAll() {
        ArrayList<LiveEvent> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM live_events");
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next())
            {
                LiveEvent event = new LiveEvent(
                        resultSet.getLong("id"),
                        resultSet.getString("title"),
                        resultSet.getString("description"),
                        resultSet.getTimestamp("date").toLocalDateTime(),
                        resultSet.getString("file_location")
                );
                res.add(event);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public LiveEvent save(LiveEvent entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            String query = "INSERT INTO live_events (file_location , title , description , date) VALUES ('" + entity.getFileLocation() +
                    "' , '" + entity.getTitle() + "' , '" +entity.getDescription() + "' , '" + entity.getDate() +"')";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public LiveEvent delete(Long aLong)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            String query = "DELETE FROM live_events WHERE id =" + aLong;
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
            return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public LiveEvent update(LiveEvent entity) {
        return null;
    }
}
