package socialnetwork.repository.database;

import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.domain.messages.Message;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;

public class DbUsersToEventsRepository implements Repository<Long , EventNotification>
{
    private String url;
    private String username;
    private String password;

    public DbUsersToEventsRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override
    public EventNotification findOne(Long aLong)
    {
        EventNotification eventNotification = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users_to_events WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                eventNotification = new EventNotification(resultSet.getLong("id"), resultSet.getLong("id_user") , resultSet.getLong("id_event") ,  resultSet.getBoolean("notified"));
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return eventNotification;
    }

    @Override
    public Iterable<EventNotification> findAll()
    {
        ArrayList<EventNotification> result = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users_to_events");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                EventNotification eventNotification = new EventNotification(resultSet.getLong("id"), resultSet.getLong("id_user") , resultSet.getLong("id_event") ,  resultSet.getBoolean("notified"));
                result.add(eventNotification);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public EventNotification save(EventNotification entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users_to_events (id_event , id_user , notified) VALUES (" +
                    entity.getId_event()+", " +
                    entity.getId_user()+", " +
                    entity.getNotified()+")");
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public EventNotification delete(Long aLong)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM users_to_events WHERE id=" + aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public EventNotification update(EventNotification entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("update users_to_events set notified=" + entity.getNotified() + " where id = " + entity.getId());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
