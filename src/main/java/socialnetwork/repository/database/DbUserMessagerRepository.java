package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.messages.Message;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;

public class DbUserMessagerRepository implements Repository<Long , Tuple<Long , Long>>
{
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public DbUserMessagerRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public Iterable<Long> findMessagesForUser(Long id_user)
    {
        ArrayList<Long> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages_to_users WHERE id_user=" + id_user);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                res.add(resultSet.getLong("id_message"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    public Iterable<Long> findUsersForMessage(Long id_message)
    {
        ArrayList<Long> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages_to_users WHERE id_message=" + id_message);
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                res.add(resultSet.getLong("id_user"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Tuple<Long, Long> findOne(Long aLong)
    {
        Tuple<Long , Long> entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages_to_users WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                entity = new Tuple<>(resultSet.getLong("id_message") , resultSet.getLong("id_user"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<Tuple<Long, Long>> findAll()
    {
        ArrayList<Tuple<Long,Long>> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages_to_users");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Tuple<Long,Long> entity = new Tuple<>(resultSet.getLong("id_message") , resultSet.getLong("id_user"));
                res.add(entity);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Tuple<Long, Long> save(Tuple<Long, Long> entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO messages_to_users VALUES ("+
                    entity.getLeft()+", "+
                    entity.getRight()+")");
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Tuple<Long, Long> delete(Long aLong)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM messages_to_user WHERE id="+aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Tuple<Long, Long> update(Tuple<Long, Long> entity)
    {
        return null;
    }
}
