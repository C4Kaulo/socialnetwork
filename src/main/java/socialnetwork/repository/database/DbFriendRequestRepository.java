package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;

public class DbFriendRequestRepository implements Repository<Long , FriendRequest>
{
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public DbFriendRequestRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }
    @Override
    public FriendRequest findOne(Long aLong)
    {
        FriendRequest entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_request WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                entity = new FriendRequest(
                        resultSet.getLong("from"),
                        resultSet.getLong("to"),
                        resultSet.getString("status"));
                entity.setEntity(resultSet.getLong("id"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<FriendRequest> findAll()
    {
        ArrayList<FriendRequest>res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_request ");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                FriendRequest entity;
                entity = new FriendRequest(
                        resultSet.getLong("from"),
                        resultSet.getLong("to"),
                        resultSet.getString("status"));
                entity.setEntity(resultSet.getLong("id"));
                res.add(entity);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public FriendRequest save(FriendRequest entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO friend_request (\"from\",\"to\",status) VALUES("+
                    entity.getFrom()+", "+
                    entity.getTo()+", '"+
                    entity.getStatus()+"')");
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public FriendRequest delete(Long aLong)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM friend_request WHERE id="+aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest update(FriendRequest entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("UPDATE friend_request SET status='"+entity.getStatus()+"' WHERE id=" +entity.getEntity());
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }
}
