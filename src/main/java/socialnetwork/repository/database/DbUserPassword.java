package socialnetwork.repository.database;

import socialnetwork.domain.Password;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.ArrayList;

public class DbUserPassword implements Repository<Long , Password>
{
    private String url;
    private String username;
    private String password;
    private UtilizatorValidator validator;

    public DbUserPassword(String url, String username, String password, UtilizatorValidator validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Password findOne(Long aLong)
    {
        Password entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user_password WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                entity = new Password(
                        resultSet.getLong("id_user"),
                        resultSet.getString("salt"),
                        resultSet.getString("password"));
                entity.setEntity(resultSet.getLong("id"));
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<Password> findAll()
    {
        ArrayList<Password> res = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM user_password");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Password found = new Password(resultSet.getLong("id_user") ,resultSet.getString("salt") , resultSet.getString("password"));
                found.setEntity(resultSet.getLong("id"));
                res.add(found);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Password save(Password entity)
    {
        Password res = null;
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO user_password(id_user , salt , password) VALUES (" +
                    entity.getId_user() + ",'" +
                    entity.getSalt() + "','" +
                    entity.getPassword() +
                    "')");
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Password delete(Long aLong)
    {
        Password res = null;
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM user_password WHERE id=" + aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Password update(Password entity)
    {
        return null;
    }
}
