package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.Vector;

public class DbUserRepository implements Repository<Long, Utilizator> {
    private String url;
    private String username;
    private String password;
    private Validator<Utilizator> validator;

    public DbUserRepository(String url, String username, String password, Validator<Utilizator> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Utilizator findOne(Long aLong)
    {
        Utilizator entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id="+aLong);
            ResultSet results = statement.executeQuery();
            if(results.next())
            {
                entity = new Utilizator(results.getString("email"),results.getString("first_name") , results.getString("last_name"));
                entity.setEntity(results.getLong("id"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<Utilizator> findAll() {
        Vector<Utilizator> users = new Vector<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from users");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String email = resultSet.getString("email");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");

                Utilizator utilizator = new Utilizator(email , firstName, lastName);
                utilizator.setEntity(id);
                users.add(utilizator);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Utilizator save(Utilizator entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO users(email , first_name , last_name) VALUES ('" + entity.getEmail() + "','" +entity.getFirstName() +"','"+ entity.getLastName()+"')");
            statement.executeUpdate();
            return entity;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Utilizator delete(Long aLong)
    {
        Utilizator entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            entity = findOne(aLong);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id="+aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Utilizator update(Utilizator entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("UPDATE users SET email='" + entity.getEmail() + "', first_name='"+entity.getFirstName()+"' , last_name='"+entity.getLastName()+"' WHERE id="+entity.getEntity());
            int i = statement.executeUpdate();
            if (i >= 1)
                return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }
}
