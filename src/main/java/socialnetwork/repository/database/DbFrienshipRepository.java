package socialnetwork.repository.database;

import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import javax.xml.transform.Result;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DbFrienshipRepository implements Repository<Long , Prietenie>{
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public DbFrienshipRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Prietenie findOne(Long aLong)
    {
        Prietenie entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                entity = new Prietenie(
                        resultSet.getLong("user1"),
                        resultSet.getLong("user2"),
                        resultSet.getTimestamp("date").toLocalDateTime());
                entity.setId(resultSet.getLong("id"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<Prietenie> findAll()
    {
        List<Prietenie> resultList = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Long id = resultSet.getLong("id");
                Long user1 = resultSet.getLong("user1");
                Long user2 = resultSet.getLong("user2");
                Prietenie prietenie = new Prietenie(user1 , user2 ,resultSet.getTimestamp("date").toLocalDateTime());
                prietenie.setId(id);
                resultList.add(prietenie);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public Prietenie save(Prietenie entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO friendships(user1 , user2 , date) VALUES ("
                    + entity.getEntity().getLeft() + ","
                    + entity.getEntity().getRight() + ",'"
                    + entity.getDate() + "')");
            statement.executeUpdate();
            return null;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Prietenie delete(Long aLong)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url , username , password);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM friendships WHERE id=" + aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Prietenie update(Prietenie entity)
    {
        return null;
    }
}
