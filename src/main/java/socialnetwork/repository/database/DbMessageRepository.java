package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.messages.Message;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class DbMessageRepository implements Repository<Long , Message>
{
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public DbMessageRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Message findOne(Long aLong)
    {
        Message entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages WHERE id=" + aLong);
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
            {
                entity = new Message(
                        resultSet.getLong("from_user"),
                        resultSet.getTimestamp("date").toLocalDateTime(),
                        resultSet.getString("content"),
                        resultSet.getLong("original_message_id")
                );
                entity.setID(resultSet.getLong("id"));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Iterable<Message> findAll()
    {
        ArrayList<Message> result = new ArrayList<>();
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages");
            ResultSet resultSet = statement.executeQuery();
            while(resultSet.next())
            {
                Message entity = new Message(
                        resultSet.getLong("from_user"),
                        resultSet.getTimestamp("date").toLocalDateTime(),
                        resultSet.getString("content"),
                        resultSet.getLong("original_message_id")
                );
                entity.setID(resultSet.getLong("id"));
                result.add(entity);
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Message save(Message entity)
    {
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            PreparedStatement statement = connection.prepareStatement("INSERT INTO messages (from_user , date , content , original_message_id) VALUES (" +
                    entity.getFrom()+", '" +
                    entity.getDate()+"', '" +
                    entity.getContent()+"' ," +
                    entity.getOriginalMessageId()+")");
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Message delete(Long aLong)
    {
        Message entity = null;
        try
        {
            Connection connection = DriverManager.getConnection(url, username, password);
            entity = findOne(aLong);
            PreparedStatement statement = connection.prepareStatement("DELETE FROM messages WHERE id="+aLong);
            statement.executeUpdate();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public Message update(Message entity)
    {
        return null;
    }
}
