package socialnetwork.service;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.ObservableList;
import jdk.nashorn.internal.runtime.doubleconv.DtoaBuffer;
import jdk.vm.ci.meta.Local;
import socialnetwork.algorithms.BiggestCommunityAlgorithm;
import socialnetwork.algorithms.HashFunction;
import socialnetwork.domain.Password;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.dto.*;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.liveevents.EventNotification;
import socialnetwork.domain.liveevents.LiveEvent;
import socialnetwork.domain.messages.Message;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.IDValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.DbFriendRequestRepository;
import socialnetwork.repository.database.DbLiveEventRepository;
import socialnetwork.repository.database.DbUserMessagerRepository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.observer.Observer;


import java.io.FileOutputStream;
import java.lang.reflect.Array;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.LongFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UtilizatorService implements Observable<AbstractEvent> {
    private Repository<Long, Prietenie> friendshipRepository;
    private Repository<Long, Utilizator> utilizatorRepository;
    private Repository<Long, Message> messageRepository;
    private DbUserMessagerRepository recipientRepository;
    private Repository<Long, FriendRequest> friendRequestRepository;
    private Repository<Long, Password> passwordRepository;
    private PagingRepository<Long, LiveEvent> eventRepository;
    private Repository<Long, EventNotification> eventNotificationRepository;
    private List<Observer<AbstractEvent>> observers = new ArrayList<>();
    UtilizatorValidator userValidator;
    IDValidator idValidator;
    private ArrayList<Utilizator> connectedUsers;

    public UtilizatorService(Repository<Long, Utilizator> utilizatorRepository,
                             Repository<Long, Prietenie> friendshipRepository,
                             Repository<Long, Message> messageRepository,
                             DbUserMessagerRepository recipientRepository,
                             Repository<Long, FriendRequest> friendRequestRepository,
                             Repository<Long, Password> passwordRepository,
                             PagingRepository<Long, LiveEvent> liveEventRepository,
                             Repository<Long, EventNotification> eventNotificationRepository) {
        this.eventRepository = liveEventRepository;
        this.utilizatorRepository = utilizatorRepository;
        this.userValidator = new UtilizatorValidator();
        this.idValidator = new IDValidator();
        this.friendshipRepository = friendshipRepository;
        this.messageRepository = messageRepository;
        this.recipientRepository = recipientRepository;
        this.friendRequestRepository = friendRequestRepository;
        this.eventNotificationRepository = eventNotificationRepository;
        this.passwordRepository = passwordRepository;
        this.connectedUsers = new ArrayList<>();
    }

    private Iterable<Prietenie> loadFriendshipsFromRepository() {
        return friendshipRepository.findAll();
    }

    private DTOFriendRequest createDTOFriendRequestFromFriendRequest(FriendRequest friendRequest) {
        Long id = null;
        for (FriendRequest request : friendRequestRepository.findAll()) {
            if (request.isEqual(friendRequest))
                id = request.getEntity();
        }
        return new DTOFriendRequest(id, friendRequest.getFrom(), friendRequest.getTo(), utilizatorRepository.findOne(friendRequest.getFrom()).getFirstName(), friendRequest.getStatus());
    }

    private DTOUser createDTOUserFromUtilizator(Utilizator utilizator) {
        Long id = null;
        for (Utilizator utilizator1 : utilizatorRepository.findAll()) {
            if (utilizator.isEqual(utilizator1))
                id = utilizator1.getEntity();
        }
        return new DTOUser(id, utilizator.getEmail(), utilizator.getFirstName(), utilizator.getLastName());
    }

    private DTOFriendship createDTOFriendshipFromPrietenie(Prietenie prietenie) {
        Long id = null;
        for (Prietenie prietenie1 : friendshipRepository.findAll()) {
            if (prietenie.isEqual(prietenie1))
                id = prietenie1.getId();
        }
        return new DTOFriendship(id, utilizatorRepository.findOne(prietenie.getEntity().getRight()).getFirstName(), utilizatorRepository.findOne(prietenie.getEntity().getRight()).getLastName(), prietenie.getDate());
    }

    private DTOMessage createDTOMessageFromMessage(Message message) {
        Long id = null;
        for (Message message1 : messageRepository.findAll()) {
            if (message1.isEqual(message)) {
                id = message1.getEntity();
            }
        }
        return new DTOMessage(id, message.getFrom(), utilizatorRepository.findOne(message.getFrom()).getFirstName());
    }

    private Iterable<Utilizator> loadUsersFromRepository() {
        List<Utilizator> users = new ArrayList<>();
        utilizatorRepository.findAll().forEach(x -> users.add(x));
        friendshipRepository.findAll().forEach(x -> {
            Prietenie p = new Prietenie(x.getEntity().getLeft(), x.getEntity().getRight(), x.getDate());
            Utilizator u1 = null;
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getEntity() == x.getEntity().getLeft()) {
                    u1 = users.get(i);
                }
            }
            Utilizator u2 = null;
            for (int i = 0; i < users.size(); i++) {
                if (users.get(i).getEntity() == x.getEntity().getRight()) {
                    u2 = users.get(i);
                }
            }
            u1.addFriend(u2);
            u2.addFriend(u1);
        });
        return users;
    }

    private DTORetea createCommunity() {
        Vector<Utilizator> vec = new Vector<>();
        loadUsersFromRepository().forEach(x -> vec.add(x));
        return new BiggestCommunityAlgorithm(vec).returnCommunity();
    }

    private DTORetea createMaxCommunity() {
        Vector<Utilizator> vec = new Vector<>();
        loadUsersFromRepository().forEach(x -> vec.add(x));
        return new BiggestCommunityAlgorithm(vec).returnMaxCommunity();
    }

    public Utilizator removeUtilizator(Long ID) {
        idValidator.validate(ID);

        Utilizator task = utilizatorRepository.findOne(ID);

        if (task != null)
            utilizatorRepository.delete(task.getEntity());
        return task;
    }

    public Utilizator addPrietenUtilizator(Long ID_user, Long ID_friend) throws ServiceException {
        Utilizator user = utilizatorRepository.findOne(ID_user);
        Utilizator friend = utilizatorRepository.findOne(ID_friend);

        if (user == null) {
            throw new ServiceException("First user is invalid");
        }
        if (friend == null) {
            throw new ServiceException("Other user is invalid");
        }

        Prietenie prieten = new Prietenie(ID_user, ID_friend, LocalDateTime.now());
        friendshipRepository.save(prieten);
        notifyObservers(new AbstractEvent(ChangeEventType.ADD, AffectedByEvent.FRIENDSHIP, createDTOFriendshipFromPrietenie(prieten)));
        return null;
    }


    public void removeFriendUtilizator(Long id_user, Long id_friend) throws ServiceException {
        for (Prietenie prietenie : friendshipRepository.findAll()) {
            if ((prietenie.getEntity().getRight() == id_user && prietenie.getEntity().getLeft() == id_friend) ||
                    (prietenie.getEntity().getRight() == id_friend && prietenie.getEntity().getLeft() == id_user)) {
                friendshipRepository.delete(prietenie.getId());
                notifyObservers(new AbstractEvent(ChangeEventType.DELETE, AffectedByEvent.FRIENDSHIP, createDTOFriendshipFromPrietenie(prietenie)));
            }
        }

    }

    public DTORetea comunitate() {
        return createCommunity();
    }

    public DTORetea maxComunitate() {
        return createMaxCommunity();
    }

    public Iterable<DTOFriendship> allFriendships(Long id) {
        return StreamSupport.stream(loadFriendshipsFromRepository().spliterator(), false)
                .filter(x -> x.getEntity().getLeft() == id || x.getEntity().getRight() == id)
                .map(x -> new DTOFriendship(id, utilizatorRepository.findOne(x.getEntity().getRight()).getFirstName(), utilizatorRepository.findOne(x.getEntity().getRight()).getLastName(), x.getDate()))
                .collect(Collectors.toList());
    }

    public Iterable<DTOFriendship> allFriendshipsInAMonth(Long id, int month) {
        return StreamSupport.stream(loadFriendshipsFromRepository().spliterator(), false)
                .filter(x -> (x.getEntity().getLeft() == id || x.getEntity().getRight() == id) && (x.getDate().getMonth().getValue() == month))
                .map(x -> new DTOFriendship(id, utilizatorRepository.findOne(x.getEntity().getRight()).getFirstName(), utilizatorRepository.findOne(x.getEntity().getRight()).getLastName(), x.getDate()))
                .collect(Collectors.toList());
    }

    public Message createMessage(Long from, List<Long> to, String content) {
        idValidator.validate(from);
        Message message = null;
        if (utilizatorRepository.findOne(from) != null) {
            message = new Message(from, LocalDateTime.now(), content, null);
            messageRepository.save(message);
            for (Message message1 : messageRepository.findAll()) {
                message = message1;
            }
            for (Long aLong : to) {
                recipientRepository.save(new Tuple<>(message.getEntity(), aLong));
            }
        }
        return message;
    }

    public Message createMessage(Long from, List<Long> to, String content, Long originalMessageId) {
        idValidator.validate(from);
        Message message = null;
        if (utilizatorRepository.findOne(from) != null) {
            message = new Message(from, LocalDateTime.now(), content, originalMessageId);
            messageRepository.save(message);
            for (Message message1 : messageRepository.findAll()) {
                message = message1;
            }
            for (Long aLong : to) {
                recipientRepository.save(new Tuple<>(message.getEntity(), aLong));
            }
        }
        return message;
    }

    public Iterable<Message> getAllMessagesForUser(Long idUser) {
        ArrayList<Message> res = new ArrayList<>();
        for (Message message : messageRepository.findAll()) {
            recipientRepository.findUsersForMessage(message.getEntity()).forEach(x ->
            {
                if (x == idUser) {
                    res.add(message);
                }
            });
        }
        return res;
    }

    public Message replyOneMessage(Long id, Long idMessage, Long idUser, Long idToUser, String content) {
        idValidator.validate(idMessage);
        idValidator.validate(idUser);
        Message message = null;
        if (utilizatorRepository.findOne(idUser) != null) {
            message = new Message(idUser, LocalDateTime.now(), content, messageRepository.findOne(idMessage).getEntity());
            messageRepository.save(message);
            recipientRepository.save(new Tuple<>(idUser, idToUser));
        }
        return message;
    }

    public Message replyAllMessage(Long id, Long idMessage, Long idUser, String content) {
        idValidator.validate(idMessage);
        idValidator.validate(idUser);
        Message message = null;
        if (utilizatorRepository.findOne(idUser) != null) {
            List<Long> to = new ArrayList<>();
            recipientRepository.findUsersForMessage(idMessage).forEach(x -> {
                if (x != idUser) {
                    to.add(x);
                }
            });
            to.add(messageRepository.findOne(idMessage).getFrom());
            message = new Message(idUser, LocalDateTime.now(), content, idMessage);
            messageRepository.save(message);
            to.forEach(x -> recipientRepository.save(new Tuple<>(idUser, x)));
        }
        return message;
    }

    public Iterable<Message> conversation(Long idUser1, Long idUser2) {
        idValidator.validate(idUser1);
        idValidator.validate(idUser2);
        ArrayList<Message> res = new ArrayList<>();
        if (utilizatorRepository.findOne(idUser1) != null && utilizatorRepository.findOne(idUser2) != null) {
            ArrayList<Message> messages = new ArrayList<>();
            messageRepository.findAll().forEach(x -> {
                if (x.getFrom() == idUser1 || x.getFrom() == idUser2) {
                    messages.add(x);
                }
            });

            messages.sort(Comparator.comparing(x -> x.getDate()));
            for (Message message : messages) {
                recipientRepository.findUsersForMessage(message.getEntity()).forEach(x ->
                {
                    if (x == idUser1 || x == idUser2) {
                        res.add(message);
                    }
                });
            }
        }
        return res;
    }

    public void sendFriendRequest(Long userFrom, Long userTo) throws ServiceException {
        for (FriendRequest friendRequest : friendRequestRepository.findAll()) {
            if (friendRequest.getStatus().equals("REJECTED") || friendRequest.getStatus().equals("APPROVED"))
                continue;
            //If the friend request already exists
            if (friendRequest.getFrom() == userFrom && friendRequest.getTo() == userTo) {
                if (isFriend(userFrom, userTo)) {
                    throw new ServiceException("Friend request already sent and waiting for approval");
                }
            }

            //If the friend request already exists and is the other way around , accept it
            if (friendRequest.getTo() == userFrom && friendRequest.getFrom() == userTo) {
                handleFriendRequest(friendRequest.getEntity(), userFrom, "APPROVED");
                DTOFriendRequest fr = createDTOFriendRequestFromFriendRequest(new FriendRequest(userFrom, userTo, "PENDING"));
                DTOFriendRequest fr2 = new DTOFriendRequest(fr);
                fr2.setStatus("APPROVED");
                notifyObservers(new AbstractEvent(ChangeEventType.UPDATE, AffectedByEvent.FRIENDREQUEST, fr2, fr));
                return;
            }
        }
        for (Prietenie prietenie : friendshipRepository.findAll()) {
            // If the friendship already exists
            if ((prietenie.getEntity().getRight() == userFrom && prietenie.getEntity().getLeft() == userTo) ||
                    (prietenie.getEntity().getRight() == userTo && prietenie.getEntity().getLeft() == userFrom)) {
                throw new ServiceException("Friendship already exists");
            }
        }
        friendRequestRepository.save(new FriendRequest(userFrom, userTo, "PENDING"));
        notifyObservers(new AbstractEvent(ChangeEventType.ADD, AffectedByEvent.FRIENDREQUEST, createDTOFriendRequestFromFriendRequest(new FriendRequest(userFrom, userTo, "PENDING"))));
    }

    public void cancelFriendRequest(Long id, Long id_user) throws ServiceException {
        FriendRequest friendRequest = friendRequestRepository.findOne(id);

        if (friendRequest == null) {
            throw new ServiceException("Cannot cancel an inexsitant friend request");
        }
        if (friendRequest.getTo() == id_user) {
            throw new ServiceException("Cannot cancel a friend request that has been sent to you. You can reject it");
        }

        DTOFriendRequest fr = createDTOFriendRequestFromFriendRequest(friendRequest);
        friendRequestRepository.delete(id);
        notifyObservers(new AbstractEvent(ChangeEventType.DELETE, AffectedByEvent.FRIENDREQUEST, fr));
    }

    public void handleFriendRequest(Long id, Long id_user, String status) throws ServiceException {
        FriendRequest entity = friendRequestRepository.findOne(id);
        if (entity == null) {
            throw new ServiceException("The friend request ID doesn't exist");
        }
        if (entity.getTo() == id_user) {
            if (entity.getStatus().equals("PENDING")) {
                FriendRequest friend = new FriendRequest(entity.getFrom(), entity.getTo(), status);
                friend.setEntity(id);
                friendRequestRepository.update(friend);
                DTOFriendRequest friendRequest = createDTOFriendRequestFromFriendRequest(friend);
                DTOFriendRequest friendRequest1 = new DTOFriendRequest(friendRequest);
                friendRequest1.setStatus("PENDING");
                if (status.equals("APPROVED")) {
                    addPrietenUtilizator(entity.getFrom(), entity.getTo());
                }
                notifyObservers(new AbstractEvent(ChangeEventType.UPDATE, AffectedByEvent.FRIENDREQUEST, friendRequest, friendRequest1));
            } else {
                throw new ServiceException("Friend Request already handled");
            }
        } else {
            throw new ServiceException("Cannot handle the friend request that you sent");
        }
    }

    public Iterable<FriendRequest> getAllFriendRequests(Long idUser) {
        return friendRequestRepository.findAll();
    }

    public Iterable<DTOFriendRequest> getFriendRequest(Long id) {
        ArrayList<DTOFriendRequest> list = new ArrayList<DTOFriendRequest>();
        for (FriendRequest friendRequest : friendRequestRepository.findAll()) {
            if (friendRequest.getTo() == id || friendRequest.getFrom() == id)
                list.add(new DTOFriendRequest(friendRequest.getEntity(), friendRequest.getFrom(), friendRequest.getTo(), utilizatorRepository.findOne(friendRequest.getFrom()).getFirstName(), friendRequest.getStatus()));
        }
        return list;
    }


    public Iterable<DTOUser> getUsers(Long entity) {
        ArrayList<DTOUser> list = new ArrayList<>();
        for (Utilizator utilizator : utilizatorRepository.findAll()) {
            if (utilizator.getEntity() != entity)
                list.add(new DTOUser(utilizator.getEntity(), utilizator.getEmail(), utilizator.getFirstName(), utilizator.getLastName()));
        }
        return list;
    }


    public Utilizator getLoginRequest(String email, String password) {
        for (Utilizator utilizator : loadUsersFromRepository()) {
            if (utilizator.getEmail().equals(email)) {
                HashFunction function = new HashFunction("SHA-256");
                for (Password password1 : passwordRepository.findAll()) {
                    if (function.getHashedString(password1.getSalt() + password).equals(password1.getPassword())) {
                        connectedUsers.add(utilizator);
                        return utilizator;
                    }
                }
            }
        }
        return null;
    }

    public void getSignUpRequest(String email, String firstName, String lastName, String password) throws ServiceException {
        Iterable<Utilizator> users = utilizatorRepository.findAll();

        for (Utilizator utilizator : users) {
            if (utilizator.getEmail().equals(email)) {
                throw new ServiceException("This email is already registered");
            }
        }
        utilizatorRepository.save(new Utilizator(email, firstName, lastName));
        users = utilizatorRepository.findAll();
        Long id = -1L;
        for (Utilizator utilizator : users) {
            if (utilizator.getEmail().equals(email)) {
                id = utilizator.getEntity();
            }
        }
        if (id == -1L) {
            throw new ServiceException("Something went wrong with adding the user");
        }
        HashFunction function = new HashFunction("SHA-256");
        String salt = function.generateSalt();
        passwordRepository.save(new Password(id, salt, function.getHashedString(salt + password)));
    }

    public Iterable<DTOFriendship> getAllFriends(Long entity) {
        ArrayList<DTOFriendship> list = new ArrayList<>();
        for (Prietenie prietenie : loadFriendshipsFromRepository()) {
            if (prietenie.getEntity().getLeft() == entity) {
                list.add(new DTOFriendship(prietenie.getEntity().getRight(), utilizatorRepository.findOne(prietenie.getEntity().getRight()).getFirstName(), utilizatorRepository.findOne(prietenie.getEntity().getRight()).getLastName(), prietenie.getDate()));
            }
            if (prietenie.getEntity().getRight() == entity) {
                list.add(new DTOFriendship(prietenie.getEntity().getLeft(), utilizatorRepository.findOne(prietenie.getEntity().getLeft()).getFirstName(), utilizatorRepository.findOne(prietenie.getEntity().getLeft()).getLastName(), prietenie.getDate()));
            }
        }
        return list;
    }

    public boolean isFriend(Long id_user1, Long id_user2) {
        for (Prietenie prietenie : friendshipRepository.findAll()) {
            if ((prietenie.getEntity().getLeft() == id_user1 && prietenie.getEntity().getRight() == id_user2) ||
                    prietenie.getEntity().getLeft() == id_user2 && prietenie.getEntity().getRight() == id_user1) {
                return true;
            }
        }
        return false;
    }

    public void deleteFriendRequest(Long id_user, Long id) throws ServiceException {
        FriendRequest entity = friendRequestRepository.findOne(id);
        if (entity == null) {
            throw new ServiceException("Friend request ID not found");
        }
        if (entity.getFrom() != id_user) {
            throw new ServiceException("");
        }
        for (FriendRequest friendRequest : friendRequestRepository.findAll()) {
            if (friendRequest.getEntity() == id) {
                friendRequestRepository.delete(id);
            } else {
                //TODO : THROW VALIDATION EXCEPTION
            }
        }
    }

    public Iterable<DTOMessage> getMessages(Long entity) {
        ArrayList<DTOMessage> list = new ArrayList<>();
        boolean ok = false;
        for (Message message : getAllMessagesForUser(entity)) {
            ok = true;
            for (DTOMessage dtoMessage : list) {
                if (dtoMessage.getName().equals(utilizatorRepository.findOne(message.getFrom()).getFirstName()))
                    ok = false;
            }
            if (ok) {
                list.add(new DTOMessage(message.getEntity(), message.getFrom(), utilizatorRepository.findOne(message.getFrom()).getFirstName()));
            }
        }
        for (Message message : messageRepository.findAll()) {
            if (message.getFrom() == entity) {
                for (Long aLong : recipientRepository.findUsersForMessage(message.getEntity())) {
                    ok = true;
                    for (DTOMessage dtoMessage : list) {
                        if (dtoMessage.getName().equals(utilizatorRepository.findOne(aLong).getFirstName()))
                            ok = false;
                    }
                    if (ok) {
                        list.add(new DTOMessage(message.getEntity(), aLong, utilizatorRepository.findOne(aLong).getFirstName()));
                    }
                }
            }
        }
        return list;
    }

    public Iterable<DTOConversation> getAllConversation(ArrayList<DTOUser> participants) {
        ArrayList<DTOConversation> list = new ArrayList<>();
        ///Iterate through all messages
        for (Message message : messageRepository.findAll()) {
            ///See if for a message you have it sent by a participant
            for (DTOUser participant : participants) {
                ///If one user is found
                if (message.getFrom().equals(participant.getID())) {
                    int iterations = 0;
                    int size = 0;
                    Iterable<Long> container = recipientRepository.findUsersForMessage(message.getEntity());
                    for (Long aLong : container) {
                        size++;
                    }
                    for (DTOUser dtoUser : participants) {
                        iterations++;
                        if (dtoUser.getID().equals(participant.getID()))
                            continue;
                        boolean found = false;
                        for (Long aLong : container) {
                            if (dtoUser.getID().equals(aLong))
                                found = true;
                        }
                        if (found == false) {
                            break;
                        }
                    }
                    if (iterations - 1 == size) {
                        if (message.getOriginalMessageId() == 0)
                            list.add(new DTOConversation(participant, message));
                        else {
                            Message mes = messageRepository.findOne(message.getOriginalMessageId());
                            Utilizator user = utilizatorRepository.findOne(mes.getFrom());
                            list.add(new DTOConversation(participant, message, new DTOConversation(createDTOUserFromUtilizator(user), mes)));
                        }
                    }
                }

            }
        }
        return list;
    }

    public boolean hasPendingRequest(Long id_user1, Long id_user2) {
        for (FriendRequest friendRequest : friendRequestRepository.findAll()) {
            if (friendRequest.getStatus().equals("PENDING") && friendRequest.getFrom().equals(id_user1) && friendRequest.getTo().equals(id_user2)) {
                return true;
            }
        }
        return false;
    }

    public FriendRequest findFriendRequest(Long id_friendRequest) {
        return friendRequestRepository.findOne(id_friendRequest);
    }

    public Long getFriendRequestId(Long id_user1, Long id_user2) throws ServiceException {
        Long result = -1L;
        for (FriendRequest friendRequest : friendRequestRepository.findAll()) {
            if ((friendRequest.getFrom() == id_user1 && friendRequest.getTo() == id_user2) ||
                    (friendRequest.getFrom() == id_user2 && friendRequest.getTo() == id_user1)) {
                result = friendRequest.getEntity();
            }
        }
        if (result.equals(-1L)) {
            throw new ServiceException("The friendship does not actually exist");
        }
        return result;
    }

    @Override
    public void addObserver(Observer<AbstractEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<AbstractEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(AbstractEvent t) {
        for (Observer<AbstractEvent> observer : observers) {
            observer.update(t);
        }
    }

    public DTOUser getOneUser(Long id) {
        ArrayList<DTOUser> list = new ArrayList<>();
        for (Utilizator utilizator : utilizatorRepository.findAll()) {
            if (utilizator.getEntity() == id)
                return new DTOUser(utilizator.getEntity(), utilizator.getEmail(), utilizator.getFirstName(), utilizator.getLastName());
        }
        return null;
    }

    public void notificationsOn(Long idNot , Long idUser, Long idEvent)
    {
        eventNotificationRepository.update(new EventNotification(idNot, idUser , idEvent , true));
        notifyObservers(new AbstractEvent(ChangeEventType.ADD , AffectedByEvent.EVENT , eventNotificationRepository.findOne(idNot)));
    }

    public void notificationsOff(Long idNot , Long idUser, Long idEvent)
    {
        eventNotificationRepository.update(new EventNotification(idNot , idUser , idEvent , false));
        notifyObservers(new AbstractEvent(ChangeEventType.DELETE , AffectedByEvent.EVENT , eventNotificationRepository.findOne(idNot)));
    }

    public void subscribeToEvent(Long idUser , Long idEvent)
    {
        eventNotificationRepository.save(new EventNotification(0L , idUser , idEvent , true));
        EventNotification ev = null;
        for (EventNotification eventNotification : eventNotificationRepository.findAll()) {
            System.out.println(eventNotification.getId() + " " + eventNotification.getId_user() + " -> " + eventNotification.getNotified());
            if(eventNotification.getId_event().equals(idEvent) && eventNotification.getId_user().equals(idUser))
            {
                ev = eventNotification;
                notificationsOn(eventNotification.getId() , eventNotification.getId_user() , eventNotification.getId_event());
            }
        }

    }

    public void unsubscribeFromEvent(Long idUser , Long idEvent)
    {
        for (EventNotification eventNotification : eventNotificationRepository.findAll())
        {
            if(eventNotification.getId_user().equals(idUser) && eventNotification.getId_event().equals(idEvent))
            {
                notificationsOff(eventNotification.getId() , eventNotification.getId_user() , eventNotification.getId_event());
                eventNotificationRepository.delete(eventNotification.getId());
            }
        }
    }

    private void updateEvents()
    {
        for (LiveEvent liveEvent : eventRepository.findAll())
        {
            if(liveEvent.getDate().isBefore(LocalDateTime.now()))
            {
                for (EventNotification eventNotification : eventNotificationRepository.findAll())
                {
                    if(eventNotification.getId_event().equals(liveEvent.getId()) && eventNotification.getNotified() == null)
                    {
                        eventNotificationRepository.update(new EventNotification(eventNotification.getId() , 0L , 0L , false));
                    }
                }
            }
        }
    }

    public void userNotified(EventNotification eventNotification)
    {
        eventNotificationRepository.update(eventNotification);
    }

    public LiveEvent getEventWithId(Long l)
    {
        return eventRepository.findOne(l);
    }

    public Iterable<EventNotification> getNotifications(Long idUser)
    {
        ArrayList<EventNotification> res = new ArrayList<>();

        for (EventNotification eventNotification : eventNotificationRepository.findAll())
        {
            System.out.println(eventNotification.getNotified());
            if(eventNotification.getId_user().equals(idUser) && !eventNotification.getNotified().equals(true))
            {
                eventNotification.setEvent(eventRepository.findOne(eventNotification.getId_event()));
                res.add(eventNotification);
            }
        }
        return res;
    }

    public Iterable<LiveEvent> getLiveEvents(int pageNumber)
    {
        ArrayList<LiveEvent> res = new ArrayList<>();
        for (LiveEvent liveEvent : this.eventRepository.findAll(new PageableImplementation(pageNumber, 5)).getContent())
        {
            for (EventNotification eventNotification : eventNotificationRepository.findAll())
            {
                if(eventNotification.getId_event().equals(liveEvent.getId()))
                {
                    liveEvent.addUser(createDTOUserFromUtilizator(utilizatorRepository.findOne(eventNotification.getId_user())));
                }
            }
            res.add(liveEvent);
        }
        return res;
    }

    public void createEvent(LiveEvent event) throws ServiceException
    {
        if(event.getDate() == null)
        {
            throw new ServiceException("The date must not be null");
        }
        if(event.getDescription().equals(""))
        {
            throw new ServiceException("The description must not be null");
        }
        if(event.getFileLocation() == null)
        {
            throw new ServiceException("The file location must not be null");
        }
        if(event.getTitle().equals(""))
        {
            throw new ServiceException("The title must not be null");
        }
        this.eventRepository.save(event);
    }

    public void generatePDFActivity(Long id, LocalDateTime from, LocalDateTime to)
    {
        try
        {
            Document document = new Document();
            PdfWriter.getInstance(document , new FileOutputStream("D:\\Anul2\\MAP\\Laborator\\Lab4-5\\generated.pdf"));
            document.open();
            document.add(new Paragraph("Friends made : \n"));
            for (Prietenie prietenie : this.friendshipRepository.findAll()) {
                if(prietenie.getDate().isAfter(from) && prietenie.getDate().isBefore(to))
                {
                    document.add(new Paragraph(prietenie.getDate() + " - " + prietenie.getDate()+"\n" ) );
                }
            }
            document.add(new Paragraph("Messages made : "));
            for (Message message : messageRepository.findAll())
            {
                if(message.getDate().isAfter(from) && message.getDate().isBefore(to) && message.getFrom().equals(id) )
                {
                    document.add(new Paragraph(message.getFrom() + " : " + message.getContent() + "\n"));
                }
            }
            document.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public ArrayList<EventNotification> getConnections(Long id)
    {
        ArrayList<EventNotification> res = new ArrayList<>();

        for (EventNotification eventNotification : eventNotificationRepository.findAll())
        {
            if(eventNotification.getId_user().equals(id))
            {
                res.add(eventNotification);
            }
        }
        return res;
    }

    public ArrayList<LiveEvent> getAllEventsForUser(Long id)
    {
        ArrayList<LiveEvent> events = new ArrayList<>();
        Iterable<EventNotification> repoEvent = eventNotificationRepository.findAll();
        for (LiveEvent liveEvent : eventRepository.findAll())
        {
            for (EventNotification eventNotification : repoEvent)
            {
                if(eventNotification.getId_event().equals(liveEvent.getId()))
                {
                    for (EventNotification notification : repoEvent) {
                        if(eventNotification.getId_event().equals(liveEvent.getId()))
                        {
                            liveEvent.addUser(createDTOUserFromUtilizator(utilizatorRepository.findOne(eventNotification.getId_user())));
                        }
                    }
                    for (DTOUser subscribedUser : liveEvent.getSubscribedUsers()) {
                        if(subscribedUser.getID().equals(id))
                        {
                            events.add(liveEvent);
                        }
                    }
                }
            }
        }
        return events;
    }

}
