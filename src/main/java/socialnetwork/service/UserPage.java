package socialnetwork.service;

import socialnetwork.domain.Password;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.dto.DTOFriendRequest;
import socialnetwork.domain.dto.DTOFriendship;
import socialnetwork.domain.dto.DTOMessage;
import socialnetwork.domain.dto.DTOUser;
import socialnetwork.domain.messages.Message;
import socialnetwork.domain.requests.FriendRequest;
import socialnetwork.domain.validators.IDValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.exceptions.ServiceException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.DbUserMessagerRepository;
import socialnetwork.utils.events.AbstractEvent;
import socialnetwork.utils.events.AffectedByEvent;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class UserPage implements Observable<AbstractEvent>
{
    private final Repository<Long , Prietenie> friendshipRepository;
    private final Repository<Long, Utilizator> utilizatorRepository;
    private Repository<Long , Message> messageRepository;
    private DbUserMessagerRepository recipientRepository;
    private Repository<Long , FriendRequest> friendRequestRepository;
    private Repository<Long , Password> passwordRepository;
    private List<Observer<AbstractEvent>> observers = new ArrayList<>();
    UtilizatorValidator userValidator;
    IDValidator idValidator;
    private ArrayList<Utilizator> connectedUsers;

    public UserPage(Repository<Long, Utilizator> utilizatorRepository , Repository<Long , Prietenie> friendshipRepository , Repository<Long , Message> messageRepository , DbUserMessagerRepository recipientRepository , Repository<Long , FriendRequest> friendRequestRepository , Repository<Long , Password> passwordRepository)
    {
        this.utilizatorRepository = utilizatorRepository;
        this.userValidator = new UtilizatorValidator();
        this.idValidator = new IDValidator();
        this.friendshipRepository = friendshipRepository;
        this.messageRepository = messageRepository;
        this.recipientRepository = recipientRepository;
        this.friendRequestRepository = friendRequestRepository;
        this.passwordRepository = passwordRepository;
        this.connectedUsers = new ArrayList<>();
    }

    private DTOFriendRequest createDTOFriendRequestFromFriendRequest(FriendRequest friendRequest)
    {
        Long id = null;
        for (FriendRequest request : friendRequestRepository.findAll())
        {
            if(request.isEqual(friendRequest))
                id = request.getEntity();
        }
        return new DTOFriendRequest(id,friendRequest.getFrom(),friendRequest.getTo(),utilizatorRepository.findOne(friendRequest.getFrom()).getFirstName(),friendRequest.getStatus());
    }

    private DTOUser createDTOUserFromUtilizator(Utilizator utilizator)
    {
        Long id = null;
        for (Utilizator utilizator1 : utilizatorRepository.findAll())
        {
            if(utilizator.isEqual(utilizator1))
                id = utilizator1.getEntity();
        }
        return new DTOUser(id,utilizator.getEmail(),utilizator.getFirstName(),utilizator.getLastName());
    }

    private DTOFriendship createDTOFriendshipFromPrietenie(Prietenie prietenie)
    {
        Long id = null;
        for (Prietenie prietenie1 : friendshipRepository.findAll())
        {
            if (prietenie.isEqual(prietenie1))
                id = prietenie1.getId();
        }
        return new DTOFriendship(id , utilizatorRepository.findOne(prietenie.getEntity().getRight()).getFirstName() , utilizatorRepository.findOne(prietenie.getEntity().getRight()).getLastName(), prietenie.getDate());
    }

    private DTOMessage createDTOMessageFromMessage(Message message)
    {
        Long id = null;
        for (Message message1 : messageRepository.findAll())
        {
            if(message1.isEqual(message))
            {
                id = message1.getEntity();
            }
        }
        return new DTOMessage(id, message.getFrom() , utilizatorRepository.findOne(message.getFrom()).getFirstName());
    }

    public Utilizator addPrietenUtilizator(Long ID_user , Long ID_friend) throws ServiceException
    {
        Utilizator user = utilizatorRepository.findOne(ID_user);
        Utilizator friend = utilizatorRepository.findOne(ID_friend);

        if(user == null)
        {
            throw new ServiceException("First user is invalid");
        }
        if(friend == null)
        {
            throw new ServiceException("Other user is invalid");
        }

        Prietenie prieten = new Prietenie(ID_user , ID_friend , LocalDateTime.now());
        friendshipRepository.save(prieten);
        notifyObservers(new AbstractEvent(ChangeEventType.ADD , AffectedByEvent.FRIENDSHIP , createDTOFriendshipFromPrietenie(prieten)));
        return null;
    }

    public Message createMessage(Long from , List<Long> to , String content)
    {
        idValidator.validate(from);
        Message message = null;
        if (utilizatorRepository.findOne(from) != null)
        {
            message = new Message(from , LocalDateTime.now() , content , null);
            messageRepository.save(message);
            for (Message message1 : messageRepository.findAll()) {
                message = message1;
            }
            for (Long aLong : to)
            {
                recipientRepository.save(new Tuple<>(message.getEntity() , aLong));
            }
        }
        return message;
    }

    public Iterable<Message> getAllMessagesForUser(Long idUser)
    {
        ArrayList<Message> res = new ArrayList<>();
        for(Message message : messageRepository.findAll())
        {
            recipientRepository.findUsersForMessage(message.getEntity()).forEach(x->
            {
                if(x == idUser)
                {
                    res.add(message);
                }
            });
        }
        return res;
    }

    public Message replyOneMessage(Long id ,Long idMessage , Long idUser , Long idToUser , String content)
    {
        idValidator.validate(idMessage);
        idValidator.validate(idUser);
        Message message = null;
        if (utilizatorRepository.findOne(idUser) != null)
        {
            message = new Message(idUser , LocalDateTime.now() , content , messageRepository.findOne(idMessage).getEntity());
            messageRepository.save(message);
            recipientRepository.save(new Tuple<>(idUser , idToUser));
        }
        return message;
    }

    public Iterable<Message> conversation(Long idUser1 , Long idUser2)
    {
        idValidator.validate(idUser1);
        idValidator.validate(idUser2);
        ArrayList<Message> res = new ArrayList<>();
        if(utilizatorRepository.findOne(idUser1) != null && utilizatorRepository.findOne(idUser2) != null)
        {
            ArrayList<Message> messages = new ArrayList<>();
            messageRepository.findAll().forEach(x->{
                if (x.getFrom() == idUser1 || x.getFrom() == idUser2)
                {
                    messages.add(x);
                }
            });

            messages.sort(Comparator.comparing(Message::getDate));
            for(Message message : messages)
            {
                recipientRepository.findUsersForMessage(message.getEntity()).forEach(x->
                {
                    if(x == idUser1 || x == idUser2)
                    {
                        res.add(message);
                    }
                });
            }
        }
        return res;
    }

    public boolean isFriend(Long id_user1 , Long id_user2)
    {
        for (Prietenie prietenie : friendshipRepository.findAll())
        {
            if ( (prietenie.getEntity().getLeft() == id_user1 && prietenie.getEntity().getRight() == id_user2) ||
                    prietenie.getEntity().getLeft() == id_user2 && prietenie.getEntity().getRight() == id_user1)
            {
                return true;
            }
        }
        return false;
    }

    public void sendFriendRequest(Long userFrom , Long userTo) throws ServiceException
    {
        for (FriendRequest friendRequest : friendRequestRepository.findAll())
        {
            if(friendRequest.getStatus().equals("REJECTED") || friendRequest.getStatus().equals("APPROVED"))
                continue;
            //If the friend request already exists
            if(friendRequest.getFrom() == userFrom && friendRequest.getTo() == userTo)
            {
                if(isFriend(userFrom , userTo))
                {
                    throw new ServiceException("Friend request already sent and waiting for approval");
                }
            }

            //If the friend request already exists and is the other way around , accept it
            if(friendRequest.getTo() == userFrom && friendRequest.getFrom() == userTo)
            {
                handleFriendRequest(friendRequest.getEntity() , userFrom , "APPROVED");
                DTOFriendRequest fr = createDTOFriendRequestFromFriendRequest(new FriendRequest(userFrom , userTo , "PENDING"));
                DTOFriendRequest fr2 = new DTOFriendRequest(fr);
                fr2.setStatus("APPROVED");
                notifyObservers(new AbstractEvent(ChangeEventType.UPDATE , AffectedByEvent.FRIENDREQUEST , fr2 , fr));
                return;
            }
        }
        for (Prietenie prietenie : friendshipRepository.findAll())
        {
            // If the friendship already exists
            if ( (prietenie.getEntity().getRight() == userFrom && prietenie.getEntity().getLeft() == userTo) ||
                    (prietenie.getEntity().getRight() == userTo && prietenie.getEntity().getLeft() == userFrom) )
            {
                throw new ServiceException("Friendship already exists");
            }
        }
        friendRequestRepository.save(new FriendRequest(userFrom , userTo , "PENDING"));
        notifyObservers(new AbstractEvent(ChangeEventType.ADD , AffectedByEvent.FRIENDREQUEST , createDTOFriendRequestFromFriendRequest(new FriendRequest(userFrom , userTo , "PENDING"))));
    }

    public void cancelFriendRequest(Long id , Long id_user)throws ServiceException
    {
        FriendRequest friendRequest = friendRequestRepository.findOne(id);

        if(friendRequest == null)
        {
            throw new ServiceException("Cannot cancel an inexsitant friend request");
        }
        if(friendRequest.getTo() == id_user)
        {
            throw new ServiceException("Cannot cancel a friend request that has been sent to you. You can reject it");
        }

        DTOFriendRequest fr = createDTOFriendRequestFromFriendRequest(friendRequest);
        friendRequestRepository.delete(id);
        notifyObservers(new AbstractEvent(ChangeEventType.DELETE , AffectedByEvent.FRIENDREQUEST , fr));
    }

    public void handleFriendRequest(Long id ,Long id_user , String status) throws ServiceException
    {
        FriendRequest entity = friendRequestRepository.findOne(id);
        if (entity == null)
        {
            throw new ServiceException("The friend request ID doesn't exist");
        }
        if( entity.getTo() == id_user )
        {
            if(entity.getStatus().equals("PENDING"))
            {
                FriendRequest friend=new FriendRequest(entity.getFrom() , entity.getTo(), status);
                friend.setEntity(id);
                friendRequestRepository.update(friend);
                DTOFriendRequest friendRequest = createDTOFriendRequestFromFriendRequest(friend);
                DTOFriendRequest friendRequest1 = new DTOFriendRequest(friendRequest);
                friendRequest1.setStatus("PENDING");
                if (status.equals("APPROVED"))
                {
                    addPrietenUtilizator(entity.getFrom() , entity.getTo());
                }
                notifyObservers(new AbstractEvent(ChangeEventType.UPDATE , AffectedByEvent.FRIENDREQUEST , friendRequest , friendRequest1));
            }
            else
            {
                throw new ServiceException("Friend Request already handled");
            }
        }
        else
        {
            throw new ServiceException("Cannot handle the friend request that you sent");
        }
    }

    @Override
    public void addObserver(Observer<AbstractEvent> e) {

    }

    @Override
    public void removeObserver(Observer<AbstractEvent> e) {

    }

    @Override
    public void notifyObservers(AbstractEvent t) {

    }
}
