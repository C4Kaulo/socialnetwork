package socialnetwork.algorithms;

import socialnetwork.domain.dto.DTORetea;
import socialnetwork.domain.Utilizator;

import java.util.HashMap;
import java.util.Vector;

public class BiggestCommunityAlgorithm {
    private Vector<Utilizator> comunity;
    private HashMap<Long , Utilizator> map;
    private HashMap<Utilizator , Long> reverse_map;
    private Vector<Vector<Long>> friends;
    private Vector<Vector<Utilizator>> all_communities;
    private boolean viz[];

    public BiggestCommunityAlgorithm(Vector<Utilizator> comunity)
    {
        this.comunity = comunity;
        this.map = new HashMap<>();
        this.reverse_map = new HashMap<>();
        this.friends = new Vector<>();
        this.all_communities = new Vector<>();
        viz = new boolean[1000];
        for(int i = 0;i < 1000;i++)
        {
            viz[i] = false;
        }
    }

    private void createMap()
    {
        Long i;
        for(i = 0L; i < comunity.size(); i++)
        {
            map.putIfAbsent(i , comunity.get(i.intValue()));
            reverse_map.putIfAbsent(comunity.get(i.intValue()) , i);
        }
    }

    private void createFriends()
    {
        Vector<Long> res = new Vector<>();
        for(Utilizator i : comunity)
        {
            if (viz[reverse_map.get(i).intValue()] == false)
            {
                res.add(reverse_map.get(i));
                viz[reverse_map.get(i).intValue()] = true;
                for(Utilizator j : i.getFriends())
                {
                    res.add(reverse_map.get(j));
                    viz[reverse_map.get(j).intValue()] = true;
                }
                friends.add(new Vector<Long>(res));
                res.clear();
            }
        }
    }

    private void createDTO()
    {
        for(Vector<Long> i : friends)
        {
            Vector<Utilizator> res = new Vector<>();
            for(Long j : i)
            {
                res.add(map.get(j));
            }
            all_communities.add(new Vector<Utilizator>(res));
            res.clear();
        }
    }

    private void createMaxDTO()
    {
        Vector<Long> coms = new Vector<>();
        int max_size = 0;
        Vector<Vector<Long>> res = new Vector<>();
        for(Vector<Long> i : friends)
        {
            if (i.size() >= max_size)
            {
                max_size = i.size();
                res.add(i);
            }
        }
        for(Vector<Long> i : res)
        {
            Vector<Utilizator> res2 = new Vector<>();
            for(Long j : i)
            {
                res2.add(map.get(j));
            }
            all_communities.add(new Vector<Utilizator>(res2));
            res2.clear();
        }
    }

    private void buildCommunity()
    {
        createMap();
        createFriends();
    }

    public DTORetea returnCommunity()
    {
        buildCommunity();
        createDTO();
        return new DTORetea(all_communities);
    }

    public DTORetea returnMaxCommunity()
    {
        buildCommunity();
        createMaxDTO();
        return new DTORetea(all_communities);
    }
}
